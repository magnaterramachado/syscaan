CREATE DATABASE syscaan
    WITH 
    OWNER = jmmagnaterra
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 32962)
-- Name: cuota; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cuota (
    id integer NOT NULL,
    cantidadcuotasanuales integer NOT NULL,
    descripcion character varying(60) NOT NULL,
    fechacreacion timestamp without time zone NOT NULL,
    observacion character varying(255),
    valorcuota double precision NOT NULL
);


--
-- TOC entry 207 (class 1259 OID 33025)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 198 (class 1259 OID 32913)
-- Name: localidad; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.localidad (
    id integer NOT NULL,
    provincia_id integer NOT NULL,
    codigopostal integer NOT NULL,
    localidad character varying(50) NOT NULL,
    prefijotelefonico integer NOT NULL,
    observacion character varying(255)
);


--
-- TOC entry 196 (class 1259 OID 32897)
-- Name: pais; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pais (
    id integer NOT NULL,
    pais character varying(50) NOT NULL,
    observacion character varying(255)
);


--
-- TOC entry 206 (class 1259 OID 32997)
-- Name: perro; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.perro (
    numlegajo integer NOT NULL,
    color character varying(255),
    edadaproximada integer,
    fechacreacion timestamp without time zone NOT NULL,
    fechaingreso timestamp without time zone NOT NULL,
    genero character(1) NOT NULL,
    nombre character varying(60) NOT NULL,
    observacion character varying(255),
    perroestado_id integer NOT NULL,
    perroraza_id integer,
    fechanacimiento timestamp without time zone NOT NULL,
    personalresponsable_id integer,
    fechaultimamodificacion timestamp without time zone NOT NULL,
    sector_id integer NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 32992)
-- Name: perroestado; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.perroestado (
    id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    fechacreacion timestamp without time zone NOT NULL,
    observacion character varying(255),
    perroactivo boolean NOT NULL,
    eliminable boolean NOT NULL,
    perroenpredio boolean NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 32987)
-- Name: perroraza; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.perroraza (
    id integer NOT NULL,
    fechacreacion timestamp without time zone NOT NULL,
    raza character varying(60) NOT NULL,
    observacion character varying(255),
    eliminable boolean NOT NULL
);


--
-- TOC entry 199 (class 1259 OID 32924)
-- Name: persona; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.persona (
    dni integer NOT NULL,
    apellido character varying(70) NOT NULL,
    direccion character varying(60) NOT NULL,
    email character varying(100),
    fechanacimiento timestamp without time zone NOT NULL,
    genero character(1) NOT NULL,
    nombre character varying(70) NOT NULL,
    telefonoalternativo character varying(30),
    telefonopersonal character varying(30) NOT NULL,
    localidad_id integer NOT NULL,
    pais_id integer NOT NULL
);


--
-- TOC entry 201 (class 1259 OID 32947)
-- Name: personal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.personal (
    activo boolean NOT NULL,
    asalariado boolean NOT NULL,
    fechaingreso timestamp without time zone NOT NULL,
    observacion character varying(255),
    salario double precision,
    dni integer NOT NULL,
    personalcargo_id integer NOT NULL,
    eliminable boolean NOT NULL,
    fechacreacion timestamp without time zone NOT NULL,
    fechaultimamodificacion timestamp without time zone NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 32939)
-- Name: personalcargo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.personalcargo (
    id integer NOT NULL,
    descripciontareas character varying(255),
    fechacreacion timestamp without time zone NOT NULL,
    nombrecargo character varying(70) NOT NULL,
    observacion character varying(255),
    comisiondirectiva boolean NOT NULL
);


--
-- TOC entry 197 (class 1259 OID 32902)
-- Name: provincia; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.provincia (
    id integer NOT NULL,
    pais_id integer NOT NULL,
    provincia character varying(70) NOT NULL,
    observacion character varying(255)
);


--
-- TOC entry 208 (class 1259 OID 40978)
-- Name: sector; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sector (
    numsector integer NOT NULL,
    descripcion character varying(255),
    fechacreacion timestamp without time zone NOT NULL,
    observacion character varying(255)
);


--
-- TOC entry 203 (class 1259 OID 32967)
-- Name: socio; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.socio (
    activo boolean NOT NULL,
    fechaasociacion timestamp without time zone NOT NULL,
    observacion character varying(255),
    dni integer NOT NULL,
    cobrador_dni integer NOT NULL,
    cuota_id integer NOT NULL,
    fechacreacion timestamp without time zone NOT NULL,
    fechaultimamodificacion timestamp without time zone NOT NULL
);


--
-- TOC entry 2747 (class 2606 OID 32966)
-- Name: cuota cuota_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cuota
    ADD CONSTRAINT cuota_pkey PRIMARY KEY (id);


--
-- TOC entry 2739 (class 2606 OID 32917)
-- Name: localidad localidad_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localidad
    ADD CONSTRAINT localidad_pkey PRIMARY KEY (id);


--
-- TOC entry 2733 (class 2606 OID 32901)
-- Name: pais pais_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 33004)
-- Name: perro perro_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perro
    ADD CONSTRAINT perro_pkey PRIMARY KEY (numlegajo);


--
-- TOC entry 2753 (class 2606 OID 32996)
-- Name: perroestado perroestado_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perroestado
    ADD CONSTRAINT perroestado_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 32991)
-- Name: perroraza perroraza_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perroraza
    ADD CONSTRAINT perroraza_pkey PRIMARY KEY (id);


--
-- TOC entry 2741 (class 2606 OID 32928)
-- Name: persona persona_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (dni);


--
-- TOC entry 2745 (class 2606 OID 32951)
-- Name: personal personal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personal
    ADD CONSTRAINT personal_pkey PRIMARY KEY (dni);


--
-- TOC entry 2743 (class 2606 OID 32946)
-- Name: personalcargo personalcargo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personalcargo
    ADD CONSTRAINT personalcargo_pkey PRIMARY KEY (id);


--
-- TOC entry 2736 (class 2606 OID 32906)
-- Name: provincia provincia_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT provincia_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 40985)
-- Name: sector sector_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sector
    ADD CONSTRAINT sector_pkey PRIMARY KEY (numsector);


--
-- TOC entry 2749 (class 2606 OID 32971)
-- Name: socio socio_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.socio
    ADD CONSTRAINT socio_pkey PRIMARY KEY (dni);


--
-- TOC entry 2737 (class 1259 OID 32923)
-- Name: fki_fk37mbpxuicwnbo878s9djjgr39; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk37mbpxuicwnbo878s9djjgr39 ON public.localidad USING btree (provincia_id);


--
-- TOC entry 2734 (class 1259 OID 32912)
-- Name: fki_fkm4s599988w0v1q1nw6dyo5t2m; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fkm4s599988w0v1q1nw6dyo5t2m ON public.provincia USING btree (pais_id);


--
-- TOC entry 2759 (class 2606 OID 32918)
-- Name: localidad fk37mbpxuicwnbo878s9djjgr39; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localidad
    ADD CONSTRAINT fk37mbpxuicwnbo878s9djjgr39 FOREIGN KEY (provincia_id) REFERENCES public.provincia(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2762 (class 2606 OID 32952)
-- Name: personal fk6cx1tcyy1u08nrjr0n5qphejr; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personal
    ADD CONSTRAINT fk6cx1tcyy1u08nrjr0n5qphejr FOREIGN KEY (personalcargo_id) REFERENCES public.personalcargo(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2769 (class 2606 OID 40960)
-- Name: perro fk705qx2vfa7p8ovk02g53ymkwt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perro
    ADD CONSTRAINT fk705qx2vfa7p8ovk02g53ymkwt FOREIGN KEY (personalresponsable_id) REFERENCES public.personal(dni);


--
-- TOC entry 2764 (class 2606 OID 32972)
-- Name: socio fk7aqje8xsbnpifkjtd7d4cbw6i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.socio
    ADD CONSTRAINT fk7aqje8xsbnpifkjtd7d4cbw6i FOREIGN KEY (cuota_id) REFERENCES public.cuota(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2765 (class 2606 OID 32977)
-- Name: socio fk9h7qlu5ireuq8htulyumhpp0f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.socio
    ADD CONSTRAINT fk9h7qlu5ireuq8htulyumhpp0f FOREIGN KEY (cobrador_dni) REFERENCES public.personal(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2766 (class 2606 OID 32982)
-- Name: socio fkak22h4pn85h1u5xnpr2bqvbhd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.socio
    ADD CONSTRAINT fkak22h4pn85h1u5xnpr2bqvbhd FOREIGN KEY (dni) REFERENCES public.persona(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2770 (class 2606 OID 40986)
-- Name: perro fkcqi3rxbkanxdi5j11i71kwg46; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perro
    ADD CONSTRAINT fkcqi3rxbkanxdi5j11i71kwg46 FOREIGN KEY (sector_id) REFERENCES public.sector(numsector);


--
-- TOC entry 2763 (class 2606 OID 32957)
-- Name: personal fki14x3yeg28csb7jwboq6ehptx; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.personal
    ADD CONSTRAINT fki14x3yeg28csb7jwboq6ehptx FOREIGN KEY (dni) REFERENCES public.persona(dni) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2767 (class 2606 OID 33010)
-- Name: perro fkkw23rt9sdf3qgxbf3kc7nxtxm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perro
    ADD CONSTRAINT fkkw23rt9sdf3qgxbf3kc7nxtxm FOREIGN KEY (perroestado_id) REFERENCES public.perroestado(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2760 (class 2606 OID 32929)
-- Name: persona fklaw6iphl585ydo6mx3bfdsqo1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT fklaw6iphl585ydo6mx3bfdsqo1 FOREIGN KEY (pais_id) REFERENCES public.pais(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2761 (class 2606 OID 32934)
-- Name: persona fklh71dr573ymt2ftrpppe7h16i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT fklh71dr573ymt2ftrpppe7h16i FOREIGN KEY (localidad_id) REFERENCES public.localidad(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2758 (class 2606 OID 32907)
-- Name: provincia fkm4s599988w0v1q1nw6dyo5t2m; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.provincia
    ADD CONSTRAINT fkm4s599988w0v1q1nw6dyo5t2m FOREIGN KEY (pais_id) REFERENCES public.pais(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2768 (class 2606 OID 33020)
-- Name: perro fkq1br0rwwt4529arjplyfelr8r; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perro
    ADD CONSTRAINT fkq1br0rwwt4529arjplyfelr8r FOREIGN KEY (perroraza_id) REFERENCES public.perroraza(id) ON UPDATE CASCADE ON DELETE RESTRICT;


-- Completed on 2019-04-21 20:24:55

--
-- PostgreSQL database dump complete
--

