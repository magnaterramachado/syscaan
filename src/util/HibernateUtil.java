package util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import view.VentanaPrincipal;

public class HibernateUtil {

	public final static String SERVER_CFG = "hibernate.cfg.xml";
	private static SessionFactory sessionFactory;
	private static String configuration;

	public static void crearSessionFactory(String configurationFile) {
		@SuppressWarnings("unused")
		org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.WARNING);
		if (sessionFactory != null && !sessionFactory.isClosed())
			sessionFactory.close();
		try {
			VentanaPrincipal.actualizarConexion(0);
			configuration = configurationFile;
			sessionFactory = new Configuration().configure(configuration).buildSessionFactory();
		}
        catch (HibernateException e) {
        	System.out.println("Hibernate Exception: " + e.getMessage());
        }
        catch (Throwable ex) {
            VentanaPrincipal.actualizarConexion(-1);
            throw new ExceptionInInitializerError(ex);
        }
		if (getConeccion())
			VentanaPrincipal.actualizarConexion(1);
		else 
			VentanaPrincipal.actualizarConexion(-1);
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static boolean getConeccion() {
		if (sessionFactory != null && !sessionFactory.isClosed())
			return true;
		return false;
	}

	public static synchronized Session getSession() {
		if (sessionFactory == null)
			sessionFactory = new Configuration().configure(configuration).buildSessionFactory();
		return sessionFactory.openSession();
	}
}