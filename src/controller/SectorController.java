package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;

import model.Sector;
import util.HibernateUtil;

public class SectorController {

	private static SectorController instance;
	private static Session session = null;

	public static SectorController getInstance() {
		if (instance == null)
			instance = new SectorController();
		return instance;
	}

	public void controlarSectoresPorDefecto() {

	}

	public boolean agregar(Sector sector) {
		session = HibernateUtil.getSession();
		if (this.existeProvincia(sector))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(sector);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public boolean eliminar(Sector sector) {
		if (!this.existeProvincia(sector)) {
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (tienePerros(sector)) {
			JOptionPane.showMessageDialog(null, "Existen perros ingresados en el sector [" + sector.getNumSector()
					+ "].\n" + "No se eliminar\u00e1 el sector.", "Acci\u00f3n imposible.", JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(sector);
			session.getTransaction().commit();
			session.close();
			return true;
		}
	}

	private boolean tienePerros(Sector sector) {
		Integer cantidad = (Integer) session.createQuery("FROM Perro AS p WHERE p.sector = :sector")
				.setParameter("sector", sector).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existeProvincia(Sector sector) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM Sector AS s WHERE s.descripcion = :descripcion")
				.setParameter("sector", sector.getDescripcion()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public Sector obtenerSector(String descripcion) {
		Sector sector = null;
		session = HibernateUtil.getSession();
		sector = (Sector) session.createQuery("FROM Sector AS s WHERE s.descripcion = :descripcion")
				.setParameter("descripcion", descripcion).uniqueResult();
		session.close();
		return sector;
	}

	public List<Sector> obtenerSectores() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Sector> sectores = session.createQuery("FROM Sector AS s ORDER BY s.numSector").list();
		session.close();
		return sectores;
	}
}