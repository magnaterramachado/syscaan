package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.PersonalCargo;
import util.HibernateUtil;

public class PersonalCargoController {

	private static PersonalCargoController instance;
	private static Session session = null;

	public static PersonalCargoController getInstance() {
		if (instance == null)
			instance = new PersonalCargoController();
		return instance;
	}

	public void controlarCargosPorDefecto() {
		PersonalCargo sinCargo = obtenerPersonalCargo("Sin cargo");
		
		if (sinCargo == null) {
			sinCargo = new PersonalCargo("Sin cargo",false);
			agregar(sinCargo);
		}
	}
	
	public boolean agregar(PersonalCargo cargo) {
		session = HibernateUtil.getSession();
		if (this.existePersonalCargo(cargo))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(cargo);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public void modificar(PersonalCargo cargo) {
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.saveOrUpdate(cargo);
		session.getTransaction().commit();
		session.close();
	}

	public boolean eliminar(PersonalCargo cargo) {
		if (!this.existePersonalCargo(cargo))
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
		else if (personalContieneCargo(cargo))
			JOptionPane.showMessageDialog(null,
					"El cargo no se puede eliminar ya que se encuentra cargado en al menos un miembro del personal.",
					"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
		else

		{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(cargo);
			session.getTransaction().commit();
			session.close();
			return true;
		}
		return false;
	}

	private boolean personalContieneCargo(PersonalCargo cargo) {
		Integer cantidad = (Integer) session.createQuery("FROM Personal AS p WHERE p.cargo = :cargo")
				.setParameter("cargo", cargo).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existePersonalCargo(PersonalCargo cargo) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM PersonalCargo AS p WHERE p.nombreCargo = :nombrecargo")
				.setParameter("nombrecargo", cargo.getNombreCargo()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public PersonalCargo obtenerPersonalCargo(String nombreCargo) {
		PersonalCargo cargo = null;
		session = HibernateUtil.getSession();
		cargo = (PersonalCargo) session.createQuery("FROM PersonalCargo WHERE nombreCargo = :nombrecargo")
				.setParameter("nombrecargo", nombreCargo).uniqueResult();
		session.close();
		return cargo;
	}

	public List<PersonalCargo> obtenerPersonalCargos() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<PersonalCargo> cargos = session.createQuery("FROM PersonalCargo AS p ORDER BY p.nombreCargo").list();
		session.close();
		return cargos;
	}
}