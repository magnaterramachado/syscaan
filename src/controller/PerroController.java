package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.Perro;
import util.HibernateUtil;

public class PerroController {

	private static PerroController instance;
	private static Session session = null;

	public static PerroController getInstance() {
		if (instance == null)
			instance = new PerroController();
		return instance;
	}

	public boolean agregar(Perro perro) {
		session = HibernateUtil.getSession();
		if (this.existePerro(perro))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(perro);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public void modificar(Perro perro) {
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.saveOrUpdate(perro);
		session.getTransaction().commit();
		session.close();
	}

	public boolean eliminar(Perro perro) {
		if (!this.existePerro(perro))
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
		else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(perro);
			session.getTransaction().commit();
			session.close();
			return true;
		}
		return false;
	}

	private boolean existePerro(Perro perro) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM Perro AS p WHERE p.numLegajo = :legajo")
				.setParameter("legajo", perro.getNumLegajo()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public List<Perro> obtenerPerros() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Perro> perro = session.createQuery("FROM Perro AS p ORDER BY p.nombre").list();
		session.close();

		return perro;
	}

	public Perro obtenerPerro(Integer legajo) {
		session = HibernateUtil.getSession();

		Perro perro = (Perro) session.createQuery("FROM Perro AS p WHERE p.numLegajo = :legajo")
				.setParameter("legajo", legajo).uniqueResult();
		session.close();

		return perro;
	}
}