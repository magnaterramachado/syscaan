package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.Pais;
import model.Provincia;
import util.HibernateUtil;

public class ProvinciaController {

	private static ProvinciaController instance;
	private static Session session = null;

	public static ProvinciaController getInstance() {
		if (instance == null)
			instance = new ProvinciaController();
		return instance;
	}

	public void controlarProvinciasPorDefecto() {
		PaisController.getInstance().controlarPaisPorDefecto();
		Pais argentina = PaisController.getInstance().obtenerPais("Argentina");

		Provincia buenosAires = obtenerProvincia("Buenos Aires", argentina);

		if (buenosAires == null) {
			buenosAires = new Provincia(argentina, "Buenos Aires");
			agregar(buenosAires);
		}
	}

	public boolean agregar(Provincia provincia) {
		session = HibernateUtil.getSession();
		if (this.existeProvincia(provincia))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(provincia);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public boolean eliminar(Provincia provincia) {
		if (!this.existeProvincia(provincia)) {
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (tieneLocalidades(provincia) && JOptionPane.showConfirmDialog(null,
				"Existen localidades contenidas dentro de la provincia \"" + provincia.getProvincia()
						+ "\" que ser\u00e1n eliminadas juntas con ella.\n�Desea continuar la operaci\u00f3n?.",
				"Confirmaci\u00f3n.", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
			return false;
		else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(provincia);
			session.getTransaction().commit();
			session.close();
			return true;
		}
	}

	private boolean tieneLocalidades(Provincia provincia) {
		Integer cantidad = (Integer) session.createQuery("FROM Localidad AS l WHERE l.provincia = :provincia")
				.setParameter("provincia", provincia).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existeProvincia(Provincia provincia) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session
				.createQuery("FROM Provincia AS p WHERE p.pais = :pais AND p.provincia = :provincia")
				.setParameter("provincia", provincia.getProvincia()).setParameter("pais", provincia.getPais()).list()
				.size();
		return (cantidad > 0 ? true : false);
	}

	public Provincia obtenerProvincia(String nombreProvincia, Pais pais) {
		Provincia provincia = null;
		session = HibernateUtil.getSession();
		provincia = (Provincia) session
				.createQuery("FROM Provincia AS p WHERE p.pais = :pais AND p.provincia = :provincia")
				.setParameter("pais", pais).setParameter("provincia", nombreProvincia).uniqueResult();
		session.close();
		return provincia;
	}

	public List<Provincia> obtenerProvincias(Pais pais) {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Provincia> provincias = session
				.createQuery("FROM Provincia AS p WHERE p.pais = :pais ORDER BY p.provincia").setParameter("pais", pais)
				.list();
		session.close();
		return provincias;
	}

	public List<Provincia> obtenerProvincias() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Provincia> provincias = session.createQuery("FROM Provincia AS p ORDER BY p.pais,p.provincia").list();
		session.close();
		return provincias;
	}

	public int cantidadProvincias(Pais pais) {
		session = HibernateUtil.getSession();
		int cantProvincias = session.createQuery("FROM Provincia AS p WHERE p.pais = :pais ORDER BY p.provincia")
				.setParameter("pais", pais).list().size();
		session.close();
		return cantProvincias;
	}
}