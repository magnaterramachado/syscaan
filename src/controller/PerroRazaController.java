package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.PerroRaza;
import util.HibernateUtil;

public class PerroRazaController {

	private static PerroRazaController instance;
	private static Session session = null;

	public static PerroRazaController getInstance() {
		if (instance == null)
			instance = new PerroRazaController();
		return instance;
	}

	public void controlarRazasPorDefecto() {
		PerroRaza sinRaza = obtenerRaza("S/R");
		
		if (sinRaza == null) {
			sinRaza = new PerroRaza("S/R");
			sinRaza.setObservacion("Sin raza");
			sinRaza.setEliminable(false);
			agregar(sinRaza);
		}
	}
	
	public boolean agregar(PerroRaza raza) {
		session = HibernateUtil.getSession();
		if (this.existeRaza(raza))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(raza);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public void modificar(PerroRaza raza) {
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.saveOrUpdate(raza);
		session.getTransaction().commit();
		session.close();
	}

	public boolean eliminar(PerroRaza raza) {
		if (!this.existeRaza(raza))
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
		else if (perroContieneRaza(raza))
			JOptionPane.showMessageDialog(null,
					"La raza no se puede eliminar ya que se encuentra cargada en al menos un perro.",
					"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
		else if (!raza.isEliminable())
			JOptionPane.showMessageDialog(null, "La raza \"S/R (Sin raza)\" no puede ser eliminada.",
					"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
		else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(raza);
			session.getTransaction().commit();
			session.close();
			return true;
		}
		return false;
	}

	private boolean perroContieneRaza(PerroRaza raza) {
		Integer cantidad = (Integer) session.createQuery("FROM Perro AS p WHERE p.raza = :raza")
				.setParameter("raza", raza).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existeRaza(PerroRaza raza) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM PerroRaza AS p WHERE p.raza = :raza")
				.setParameter("raza", raza.getRaza()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public PerroRaza obtenerRaza(String nombreRaza) {
		PerroRaza raza = null;
		session = HibernateUtil.getSession();
		raza = (PerroRaza) session.createQuery("FROM PerroRaza AS p WHERE p.raza = :raza")
				.setParameter("raza", nombreRaza).uniqueResult();
		session.close();
		return raza;
	}

	public List<PerroRaza> obtenerRazas() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<PerroRaza> razas = session.createQuery("FROM PerroRaza AS p ORDER BY p.raza").list();
		session.close();
		
		for (int i = 0; i < razas.size(); i++)
			if (razas.get(i).getRaza().equals("S/R")) {
				razas.add(0, razas.get(i));
				razas.remove(i + 1);
			}
		return razas;
	}
}