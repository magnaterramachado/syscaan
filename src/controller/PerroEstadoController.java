package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.PerroEstado;
import util.HibernateUtil;

public class PerroEstadoController {

	private static PerroEstadoController instance;
	private static Session session = null;

	public static PerroEstadoController getInstance() {
		if (instance == null)
			instance = new PerroEstadoController();
		return instance;
	}

	public void controlarEstadosPorDefecto() {
		PerroEstado muerto = obtenerEstado("Muerto");
		PerroEstado normal = obtenerEstado("Normal");

		if (muerto == null) {
			muerto = new PerroEstado("Muerto", false, false);
			muerto.setEliminable(false);
			agregar(muerto);
		}

		if (normal == null) {
			normal = new PerroEstado("Normal", true, true);
			normal.setEliminable(false);
			agregar(normal);
		}

//		List<PerroEstado> listaEstados = obtenerEstados();
//		boolean existeMuerto = false;
//		boolean existeNormal = false;
//		for (int i = 0; i < listaEstados.size(); i++) {
//			if (listaEstados.get(i).getDescripcion().equals("Muerto") && listaEstados.get(i).isPerroActivo() == false)
//				existeMuerto = true;
//			else if (listaEstados.get(i).getDescripcion().equals("Normal")
//					&& listaEstados.get(i).isPerroActivo() == true)
//				existeNormal = true;
//		}
//
//		if (!existeMuerto) {
//			PerroEstado muerto = new PerroEstado("Muerto", false, false);
//			muerto.setEliminable(false);
//			agregar(muerto);
//		}
//
//		if (!existeNormal) {
//			PerroEstado normal = new PerroEstado("Normal", true, true);
//			normal.setEliminable(false);
//			agregar(normal);
//		}
	}

	public boolean agregar(PerroEstado estado) {
		session = HibernateUtil.getSession();
		if (this.existeEstado(estado))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(estado);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public void modificar(PerroEstado estado) {
		if (!estado.isEliminable()) {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.saveOrUpdate(estado);
			session.getTransaction().commit();
			session.close();
		}
	}

	public boolean eliminar(PerroEstado estado) {
		if (!this.existeEstado(estado))
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
		else if (perroContieneEstado(estado))
			JOptionPane.showMessageDialog(null,
					"El estado no se puede eliminar ya que se encuentra cargada en al menos un perro.",
					"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
		else if (!estado.isEliminable()) {
			JOptionPane.showMessageDialog(null, "Los estados \"Normal\" y \"Muerto\" no pueden ser eliminados.",
					"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
		} else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(estado);
			session.getTransaction().commit();
			session.close();
			return true;
		}
		return false;
	}

	private boolean perroContieneEstado(PerroEstado estado) {
		Integer cantidad = (Integer) session.createQuery("FROM Perro AS p WHERE p.estado = :estado")
				.setParameter("estado", estado).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existeEstado(PerroEstado estado) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM PerroEstado AS p WHERE p.descripcion = :descripcion")
				.setParameter("descripcion", estado.getDescripcion()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public PerroEstado obtenerEstado(String descripcion) {
		PerroEstado estado = null;
		session = HibernateUtil.getSession();
		estado = (PerroEstado) session.createQuery("FROM PerroEstado WHERE descripcion = :descripcion")
				.setParameter("descripcion", descripcion).uniqueResult();
		session.close();
		return estado;
	}

	public List<PerroEstado> obtenerEstados() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<PerroEstado> estados = session.createQuery("FROM PerroEstado AS p ORDER BY p.descripcion").list();
		session.close();

		for (int i = 0; i < estados.size(); i++)
			if (estados.get(i).getDescripcion().equals("Muerto")) {
				estados.add(0, estados.get(i));
				estados.remove(i + 1);
			} else if (estados.get(i).getDescripcion().equals("Normal")) {
				estados.add(0, estados.get(i));
				estados.remove(i + 1);
			}

		return estados;
	}
}