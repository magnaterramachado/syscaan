package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.Personal;
import util.HibernateUtil;

public class PersonalController {

	private static PersonalController instance;
	private static Session session = null;

	public static PersonalController getInstance() {
		if (instance == null)
			instance = new PersonalController();
		return instance;
	}

	public boolean agregar(Personal personal) {
		session = HibernateUtil.getSession();
		if (this.existePersonal(personal))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(personal);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public void modificar(Personal personal) {
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.saveOrUpdate(personal);
		session.getTransaction().commit();
		session.close();
	}

	public boolean eliminar(Personal personal) {
		if (!this.existePersonal(personal))
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
		else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(personal);
			session.getTransaction().commit();
			session.close();
			return true;
		}
		return false;
	}

	private boolean existePersonal(Personal personal) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM Personal AS p WHERE p.dni = :dni")
				.setParameter("dni", personal.getDni()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public Personal obtenerPersonal(Integer dni) {
		Personal personal = null;
		session = HibernateUtil.getSession();
		personal = (Personal) session.createQuery("FROM Personal WHERE dni = :dni").setParameter("dni", dni)
				.uniqueResult();
		session.close();
		return personal;
	}

	public List<Personal> obtenerListaPersonal() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Personal> listaPersonal = session.createQuery("FROM Personal AS p ORDER BY p.apellido,p.nombre").list();
		session.close();
		return listaPersonal;
	}
}