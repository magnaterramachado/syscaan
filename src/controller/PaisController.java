package controller;

import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import model.Pais;
import util.HibernateUtil;

public class PaisController {

	private static PaisController instance;
	private static Session session = null;

	public static PaisController getInstance() {
		if (instance == null)
			instance = new PaisController();
		return instance;
	}

	public void controlarPaisPorDefecto() {
		Pais argentina = obtenerPais("Argentina");

		if (argentina == null) {
			argentina = new Pais("Argentina");
			agregar(argentina);
		}
	}

	public boolean agregar(Pais pais) {
		session = HibernateUtil.getSession();
		if (this.existePais(pais))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(pais);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public boolean eliminar(Pais pais) {
		if (!this.existePais(pais)) {
			JOptionPane.showMessageDialog(null, "El objeto no existe en la base de datos.", "Acci\u00f3n imposible.",
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else if (tieneProvincias(pais) && JOptionPane.showConfirmDialog(null,
				"Existen provincias contenidas dentro del pa\u00eds \"" + pais.getPais()
						+ "\" que ser\u00e1n eliminadas juntas con el.\n�Desea continuar la operaci\u00f3n?.",
				"Confirmaci\u00f3n.", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
			return false;
		else {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.delete(pais);
			session.getTransaction().commit();
			session.close();
			return true;
		}
	}

	private boolean tieneProvincias(Pais pais) {
		Integer cantidad = (Integer) session.createQuery("FROM Provincia AS p WHERE p.pais = :pais")
				.setParameter("pais", pais).list().size();
		return (cantidad > 0 ? true : false);
	}

	private boolean existePais(Pais pais) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session.createQuery("FROM Pais AS p WHERE p.pais = :pais")
				.setParameter("pais", pais.getPais()).list().size();
		return (cantidad > 0 ? true : false);
	}

	public Pais obtenerPais(String nombrePais) {
		Pais pais = null;
		session = HibernateUtil.getSession();
		pais = (Pais) session.createQuery("FROM Pais AS p WHERE p.pais = :pais").setParameter("pais", nombrePais)
				.uniqueResult();
		session.close();
		return pais;
	}

	public List<Pais> obtenerPaises() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Pais> paises = session.createQuery("FROM Pais AS p ORDER BY p.pais").list();
		session.close();
		return paises;
	}
}