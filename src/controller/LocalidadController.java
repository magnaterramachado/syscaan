package controller;

import java.util.List;
import org.hibernate.Session;
import model.Provincia;
import model.Localidad;
import model.Pais;
import util.HibernateUtil;

public class LocalidadController {

	private static LocalidadController instance;
	private static Session session = null;

	public static LocalidadController getInstance() {
		if (instance == null)
			instance = new LocalidadController();
		return instance;
	}

	public void controlarLocalidadesPorDefecto() {
		ProvinciaController.getInstance().controlarProvinciasPorDefecto();
		Pais argentina = PaisController.getInstance().obtenerPais("Argentina");
		Provincia buenosAires = ProvinciaController.getInstance().obtenerProvincia("Buenos Aires",argentina);
	
		Localidad necochea = obtenerLocalidad("Necochea", buenosAires);

		if (necochea == null) {
			necochea = new Localidad(buenosAires,"Necochea","7630","02262");
			agregar(necochea);
		}
	}
	
	public boolean agregar(Localidad localidad) {
		session = HibernateUtil.getSession();
		if (this.existeLocalidad(localidad))
			return false;
		session.beginTransaction();
		session.saveOrUpdate(localidad);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	public boolean eliminar(Localidad localidad) {
		if (!this.existeLocalidad(localidad))
			return false;
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.delete(localidad);
		session.getTransaction().commit();
		session.close();
		return true;
	}

	private boolean existeLocalidad(Localidad localidad) {
		session = HibernateUtil.getSession();
		Integer cantidad = (Integer) session
				.createQuery("FROM Localidad AS l WHERE l.provincia = :provincia AND l.localidad = :localidad")
				.setParameter("localidad", localidad.getLocalidad()).setParameter("provincia", localidad.getProvincia())
				.list().size();
		return (cantidad > 0 ? true : false);
	}

	public Localidad obtenerLocalidad(String nombreLocalidad, Provincia provincia) {
		Localidad localidad = null;
		session = HibernateUtil.getSession();
		localidad = (Localidad) session
				.createQuery("FROM Localidad AS l WHERE l.provincia = :provincia AND l.localidad = :localidad")
				.setParameter("provincia", provincia).setParameter("localidad", nombreLocalidad).uniqueResult();
		session.close();
		return localidad;
	}

	public List<Localidad> obtenerLocalidades(Provincia provincia) {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Localidad> localidades = session
				.createQuery("FROM Localidad AS l WHERE l.provincia = :provincia ORDER BY l.localidad")
				.setParameter("provincia", provincia).list();
		session.close();
		return localidades;
	}

	public List<Localidad> obtenerLocalidades() {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Localidad> localidades = session.createQuery("FROM Localidad AS l ORDER BY l.provincia,l.localidad")
				.list();
		session.close();
		return localidades;
	}

	public int cantidadLocalidades(Provincia provincia) {
		session = HibernateUtil.getSession();

		@SuppressWarnings("unchecked")
		List<Localidad> localidades = session
				.createQuery("FROM Localidad AS l WHERE l.provincia = :provincia ORDER BY l.localidad")
				.setParameter("provincia", provincia).list();
		session.close();
		return localidades.size();
	}
}