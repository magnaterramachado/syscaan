package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sector")
public class Sector implements Serializable {

	private static final long serialVersionUID = -5502942469055099331L;
	private Integer numSector;
	private Date fechaCreacion;
	private String descripcion;
	private String observacion;

	public Sector() {

	}

	@Id
	public Integer getNumSector() {
		return numSector;
	}

	public void setNumSector(Integer numSector) {
		this.numSector = numSector;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Sector))
			return false;

		if (this == obj)
			return true;

		Sector tmpSector = (Sector) obj;

		if (this.getNumSector().equals(tmpSector.getNumSector())) {

			if ((this.getDescripcion() == null && tmpSector.getDescripcion() != null)
					|| (this.getDescripcion() != null && tmpSector.getDescripcion() == null))
				return false;

			if ((this.getObservacion() == null && tmpSector.getObservacion() != null)
					|| (this.getObservacion() != null && tmpSector.getObservacion() == null))
				return false;

			else if ((this.getObservacion() == null && tmpSector.getObservacion() == null)
					|| (this.getObservacion().equals(tmpSector.getObservacion()))
							&& (this.getDescripcion() == null && tmpSector.getDescripcion() == null)
					|| (this.getDescripcion().equals(tmpSector.getDescripcion())))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String etiqueta = "[" + numSector + "] ";
		if (descripcion != null)
			etiqueta += descripcion;
		return etiqueta;
	}
}