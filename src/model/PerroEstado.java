package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "perroestado")
public class PerroEstado implements Serializable {

	private static final long serialVersionUID = -7162350714265880701L;
	private Integer id;
	private Date fechaCreacion;
	private String descripcion;
	private boolean perroActivo;
	private boolean perroEnPredio;
	private boolean eliminable;
	private String observacion;
	// Este atributo transiente sirve para mostrar en el método toString()
	// una descripción detallada o bien solo la descripción del estado
	private String etiqueta;

	public PerroEstado() {
		fechaCreacion = new Date();
		this.eliminable = true;
	}

	public PerroEstado(String descripcion, boolean perroActivo, boolean perroEnPredio) {
		fechaCreacion = new Date();
		this.descripcion = descripcion;
		this.perroActivo = perroActivo;
		this.perroEnPredio = perroEnPredio;
		this.eliminable = true;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 50)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = true)
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(nullable = false)
	public boolean isPerroActivo() {
		return perroActivo;
	}

	public void setPerroActivo(boolean perroActivo) {
		this.perroActivo = perroActivo;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(nullable = false)
	public boolean isEliminable() {
		return eliminable;
	}

	public void setEliminable(boolean eliminable) {
		this.eliminable = eliminable;
	}

	@Column(nullable = false)
	public boolean isPerroEnPredio() {
		return perroEnPredio;
	}

	public void setPerroEnPredio(boolean perroEnPredio) {
		this.perroEnPredio = perroEnPredio;
	}

	@Transient
	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(boolean decision) {
		this.etiqueta = this.descripcion;
		if (decision) {
			if (getObservacion() != null)
				this.etiqueta = this.etiqueta + "    (" + getObservacion() + ")";
			if (perroActivo == true) {
				this.etiqueta = this.etiqueta + "    [activo]";
				if (perroEnPredio == true)
					this.etiqueta = this.etiqueta + "    [en predio]";
				else
					this.etiqueta = this.etiqueta + "    [no en predio]";
			} else
				this.etiqueta = this.etiqueta + "    [inactivo]";
		} else {
			if (perroActivo == true)
				this.etiqueta = this.etiqueta + "    [activo]";
			else
				this.etiqueta = this.etiqueta + "    [inactivo]";
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PerroEstado))
			return false;

		if (this == obj)
			return true;

		PerroEstado tmpEstado = (PerroEstado) obj;

		if (this.getId().equals(tmpEstado.getId()) && this.getDescripcion().equals(tmpEstado.getDescripcion())
				&& this.isPerroActivo() == tmpEstado.perroActivo && this.isPerroEnPredio() == tmpEstado.perroEnPredio) {
			if ((this.getObservacion() == null && tmpEstado.getObservacion() != null)
					|| (this.getObservacion() != null && tmpEstado.getObservacion() == null))
				return false;
			else if ((this.getObservacion() == null && tmpEstado.getObservacion() == null)
					|| (this.getObservacion().equals(tmpEstado.getObservacion())))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		if (this.etiqueta == null)
			return this.descripcion;
		return this.etiqueta;
	}
}