package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cuota")
public class Cuota implements Serializable {

	private static final long serialVersionUID = -5502942469055099331L;
	private Integer id;
	private Date fechaCreacion;
	private String descripcion;
	private Integer cantidadCuotasAnuales;
	private Double valorCuota;
	private String observacion;

	public Cuota() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Column(nullable = false, length=60)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(nullable = false)
	public Integer getCantidadCuotasAnuales() {
		return cantidadCuotasAnuales;
	}

	public void setCantidadCuotasAnuales(Integer cantidadCuotasAnuales) {
		this.cantidadCuotasAnuales = cantidadCuotasAnuales;
	}

	@Column(nullable = false)
	public Double getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}