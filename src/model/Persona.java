package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona implements Serializable {

	private static final long serialVersionUID = -6274702886983415656L;
	protected Integer dni;
	protected Character genero;
	protected String apellido;
	protected String nombre;
	protected Date fechaNacimiento;
	protected Pais nacionalidad;
	protected Localidad localidadResidencia;
	protected String direccion;
	protected String telefonoPersonal;
	protected String telefonoAlternativo;
	protected String email;

	public Persona() {
	}
	
	public Persona(Integer dni, String apellido, String nombre) {
		this.dni = dni;
		this.apellido = apellido;
		this.nombre = nombre;
	}

	public Persona(Integer dni, Character genero, String apellido, String nombre, Date fechaNacimiento,
			Pais nacionalidad, Localidad localidadResidencia, String direccion, String telefonoPersonal) {
		this.dni = dni;
		this.genero = genero;
		this.apellido = apellido;
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
		this.nacionalidad = nacionalidad;
		this.localidadResidencia = localidadResidencia;
		this.direccion = direccion;
		this.telefonoPersonal = telefonoPersonal;
	}

	@Id
	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	@Column(nullable = false)
	public Character getGenero() {
		return genero;
	}

	public void setGenero(Character genero) {
		this.genero = genero;
	}

	@Column(nullable = false, length = 70)
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Column(nullable = false, length = 70)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@ManyToOne
	@JoinColumn(name = "pais_id", referencedColumnName = "id", nullable = false)
	public Pais getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Pais nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	@ManyToOne
	@JoinColumn(name = "localidad_id", referencedColumnName = "id", nullable = false)
	public Localidad getLocalidadResidencia() {
		return localidadResidencia;
	}

	public void setLocalidadResidencia(Localidad localidadResidencia) {
		this.localidadResidencia = localidadResidencia;
	}

	@Column(nullable = false, length = 60)
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Column(nullable = false, length = 30)
	public String getTelefonoPersonal() {
		return telefonoPersonal;
	}

	public void setTelefonoPersonal(String telefonoPersonal) {
		this.telefonoPersonal = telefonoPersonal;
	}

	@Column(length = 30)
	public String getTelefonoAlternativo() {
		return telefonoAlternativo;
	}

	public void setTelefonoAlternativo(String telefonoAlternativo) {
		this.telefonoAlternativo = telefonoAlternativo;
	}

	@Column(length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || !(obj instanceof Persona))
			return false;
		if (this == obj)
			return true;
		Persona persona = (Persona) obj;
		if (this.dni != persona.dni)
			return false;
		return true;
	}
}