package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "socio")
@PrimaryKeyJoinColumn(name = "dni")
public class Socio extends Persona implements Serializable {

	private static final long serialVersionUID = -4937682539911126285L;
	private Date fechaCreacion;
	private Date fechaUltimaModificacion;
	private Date fechaAsociacion;
	private Personal cobrador;
	private Cuota cuota;
	private boolean activo;
	private String observacion;

	public Socio() {
	}
	
	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Column(nullable = false)
	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}
	
	@Column(nullable = false)
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Column(nullable = false)
	public Date getFechaAsociacion() {
		return fechaAsociacion;
	}

	public void setFechaAsociacion(Date fechaAsociacion) {
		this.fechaAsociacion = fechaAsociacion;
	}
	
	@ManyToOne
	@JoinColumn(name = "cobrador_dni", referencedColumnName = "dni", nullable = false)
	public Personal getCobrador() {
		return cobrador;
	}

	public void setCobrador(Personal cobrador) {
		this.cobrador = cobrador;
	}
	
	@ManyToOne
	@JoinColumn(name = "cuota_id", referencedColumnName = "id", nullable = false)
	public Cuota getCuota() {
		return cuota;
	}

	public void setCuota(Cuota cuota) {
		this.cuota = cuota;
	}
	
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}