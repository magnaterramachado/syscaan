package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "personalcargo")
public class PersonalCargo implements Serializable {

	private static final long serialVersionUID = -484532232519155963L;
	private Integer id;
	private Date fechaCreacion;
	private String nombreCargo;
	private boolean comisionDirectiva;
	private String descripcionTareas;
	private String observacion;

	public PersonalCargo() {
	}

	public PersonalCargo(String nombreCargo, boolean comisionDirectiva) {
		this.fechaCreacion = new Date();
		this.nombreCargo = nombreCargo;
		this.comisionDirectiva = comisionDirectiva;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 70)
	public String getNombreCargo() {
		return nombreCargo;
	}

	public void setNombreCargo(String nombreRelacion) {
		this.nombreCargo = nombreRelacion;
	}

	public String getDescripcionTareas() {
		return descripcionTareas;
	}

	public void setDescripcionTareas(String descripcionTareas) {
		this.descripcionTareas = descripcionTareas;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(nullable = false)
	public boolean isComisionDirectiva() {
		return comisionDirectiva;
	}

	public void setComisionDirectiva(boolean comisionDirectiva) {
		this.comisionDirectiva = comisionDirectiva;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PersonalCargo))
			return false;

		if (this == obj)
			return true;

		PersonalCargo tmpCargo = (PersonalCargo) obj;

		if (this.getId().equals(tmpCargo.getId()) && this.getNombreCargo().equals(tmpCargo.getNombreCargo())
				&& this.isComisionDirectiva() == tmpCargo.isComisionDirectiva()) {
			
			if ((this.getDescripcionTareas() == null && tmpCargo.getDescripcionTareas() != null)
					|| (this.getDescripcionTareas() == null) && tmpCargo.getDescripcionTareas() != null)
				return false;
			if ((this.getObservacion() == null && tmpCargo.getObservacion() != null)
					|| (this.getObservacion() != null && tmpCargo.getObservacion() == null))
				return false;
			
			else if (((this.getObservacion() == null && tmpCargo.getObservacion() == null)
					|| (this.getObservacion().equals(tmpCargo.getObservacion()))) && 
					((this.getDescripcionTareas() == null && tmpCargo.getDescripcionTareas() == null)
							|| (this.getDescripcionTareas().equals(tmpCargo.getDescripcionTareas()))))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		if (isComisionDirectiva())
			return getNombreCargo() + " [Comisi\u00f3n Directiva]";
		else
			return getNombreCargo();
	}
}