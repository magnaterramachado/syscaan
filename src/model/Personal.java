package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "personal")
@PrimaryKeyJoinColumn(name = "dni")
public class Personal extends Persona implements Serializable {

	private static final long serialVersionUID = -2379288366553385041L;
	private Date fechaCreacion;
	private Date fechaUltimaModificacion;
	private PersonalCargo cargo;
	private boolean asalariado;
	private Double salario;
	private Date fechaIngreso;
	private boolean activo;
	private boolean eliminable;
	private String observacion;

	public Personal() {
		super();
		this.eliminable = true;
	}

	public Personal(Integer dni, String apellido, String nombre) {
		super(dni, apellido, nombre);
	}

	public Personal(Integer dni, Character genero, String apellido, String nombre, Date fechaNacimiento,
			Pais nacionalidad, Localidad localidadResidencia, String direccion, String telefonoPersonal,
			PersonalCargo cargo, boolean activo, boolean asalariado, Date fechaIngreso) {
		super(dni, genero, apellido, nombre, fechaNacimiento, nacionalidad, localidadResidencia, direccion,
				telefonoPersonal);
		this.setFechaCreacion(new Date());
		this.setFechaUltimaModificacion(new Date());
		this.eliminable = true;
		this.activo = activo;
		this.cargo = cargo;
		this.asalariado = asalariado;
		this.fechaIngreso = fechaIngreso;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(nullable = false)
	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	@ManyToOne
	@JoinColumn(name = "personalcargo_id", referencedColumnName = "id", nullable = false)
	public PersonalCargo getCargo() {
		return cargo;
	}

	public void setCargo(PersonalCargo cargo) {
		this.cargo = cargo;
	}

	@Column(nullable = false)
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Column(nullable = false)
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@Column(nullable = false)
	public boolean isAsalariado() {
		return asalariado;
	}

	public void setAsalariado(boolean asalariado) {
		this.asalariado = asalariado;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(nullable = false)
	public boolean isEliminable() {
		return eliminable;
	}

	public void setEliminable(boolean eliminable) {
		this.eliminable = eliminable;
	}

	@Override
	public String toString() {
		return "[" + super.dni.toString() + "]" + " " + super.apellido + " " + super.nombre;
	}
}