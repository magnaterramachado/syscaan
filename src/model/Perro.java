package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "perro")
public class Perro implements Serializable {

	private static final long serialVersionUID = -8211879356839582053L;
	private Integer numLegajo;
	private Date fechaCreacion;
	private Date fechaUltimaModificacion;
	private Date fechaIngreso;
	private Sector sector;
	private String nombre;
	private Character genero;
	private Date fechaNacimiento;
	private Date fechaBaja;
	private String color;
	private PerroRaza raza;
	private Personal responsable;
	private String observacion;
	private PerroEstado estado;

	public Perro() {
	}

	public Perro(Integer numLegajo, Date fechaIngreso, String nombre, Character genero, PerroRaza raza,
			PerroEstado estado, Sector sector, Date fechaNacimiento) {
		this.fechaCreacion = new Date();
		this.fechaUltimaModificacion = new Date();
		this.numLegajo = numLegajo;
		this.fechaIngreso = fechaIngreso;
		this.nombre = nombre;
		this.genero = genero;
		this.sector = sector;
		this.raza = raza;
		this.estado = estado;
		this.fechaNacimiento = fechaNacimiento;
	}

	@Id
	public Integer getNumLegajo() {
		return numLegajo;
	}

	public void setNumLegajo(Integer numLegajo) {
		this.numLegajo = numLegajo;
	}

	@ManyToOne
	@JoinColumn(name = "sector_id", referencedColumnName = "numsector", nullable = false)
	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	@Column(nullable = false)
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@Column(nullable = false, length = 60)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public Character getGenero() {
		return genero;
	}

	public void setGenero(Character genero) {
		this.genero = genero;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@ManyToOne
	@JoinColumn(name = "perroraza_id", referencedColumnName = "id", nullable = false)
	public PerroRaza getRaza() {
		return raza;
	}

	public void setRaza(PerroRaza raza) {
		this.raza = raza;
	}

	@ManyToOne
	@JoinColumn(name = "personalresponsable_id", referencedColumnName = "dni", nullable = true)
	public Personal getResponsable() {
		return responsable;
	}

	public void setResponsable(Personal responsable) {
		this.responsable = responsable;
	}

	@ManyToOne
	@JoinColumn(name = "perroestado_id", referencedColumnName = "id", nullable = false)
	public PerroEstado getEstado() {
		return estado;
	}

	public void setEstado(PerroEstado estado) {
		this.estado = estado;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(nullable = false)
	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(nullable = false)
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Column(nullable = true)
	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Perro))
			return false;

		if (this == obj)
			return true;

		Perro tmpPerro = (Perro) obj;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		if (this.getNumLegajo().equals(tmpPerro.getNumLegajo()) && this.getNombre().equals(tmpPerro.getNombre())
				&& df.format(this.getFechaIngreso()).equals(df.format(tmpPerro.getFechaIngreso()))
				&& df.format(this.getFechaNacimiento()).equals(df.format(tmpPerro.getFechaNacimiento()))
				&& this.getSector().equals(tmpPerro.getSector()) && this.getGenero().equals(tmpPerro.getGenero())
				&& this.getEstado().equals(tmpPerro.getEstado()) && this.getRaza().equals(tmpPerro.getRaza())) {

			if ((this.getFechaBaja() == null && tmpPerro.getFechaBaja() != null)
					|| (this.getFechaBaja() != null && tmpPerro.getFechaBaja() == null))
				return false;

			if ((this.getObservacion() == null && tmpPerro.getObservacion() != null)
					|| (this.getObservacion() != null && tmpPerro.getObservacion() == null))
				return false;

			if ((this.getColor() == null && tmpPerro.getColor() != null)
					|| (this.getColor() != null && tmpPerro.getColor() == null))
				return false;

			if ((this.getResponsable() == null && tmpPerro.getResponsable() != null)
					|| (this.getResponsable() != null && tmpPerro.getResponsable() == null))
				return false;

			if (((this.getObservacion() == null && tmpPerro.getObservacion() == null)
					|| (this.getObservacion().equals(tmpPerro.getObservacion())))
					&& ((this.getColor() == null && tmpPerro.getColor() == null)
							|| (this.getColor().equals(tmpPerro.getColor())))
					&& ((this.getResponsable() == null && tmpPerro.getResponsable() == null)
							|| (this.getResponsable().equals(tmpPerro.getResponsable())))
					&& ((this.getFechaBaja() == null && tmpPerro.getFechaBaja() == null)
							|| df.format(this.getFechaBaja()).equals(df.format(tmpPerro.getFechaBaja()))))
				return true;
		}
		return false;
	}
}