package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "perroraza")
public class PerroRaza implements Serializable {

	private static final long serialVersionUID = 4099699868520594307L;
	private Integer id;
	private Date fechaCreacion;
	private String raza;
	private String observacion;
	private boolean eliminable;
	// Este atributo transiente sirve para mostrar en el m�todo toString()
	// una descripci�n detallada o bien solo la descripci�n de la raza
	private String etiqueta;

	public PerroRaza() {
		fechaCreacion = new Date();
		this.eliminable = true;
	}

	public PerroRaza(String raza) {
		fechaCreacion = new Date();
		this.raza = raza;
		this.eliminable = true;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(nullable = false, length = 60)
	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(nullable = false)
	public boolean isEliminable() {
		return eliminable;
	}

	public void setEliminable(boolean eliminable) {
		this.eliminable = eliminable;
	}

	@Transient
	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(boolean decision) {
		this.etiqueta = this.raza;
		if (decision) {
			if (getObservacion() != null)
				this.etiqueta = this.etiqueta + "    (" + getObservacion() + ")";
		}
	}

	@Override
	public String toString() {
		if (this.etiqueta == null)
			return this.raza;
		return this.etiqueta;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PerroRaza))
			return false;

		if (this == obj)
			return true;

		PerroRaza tmpRaza = (PerroRaza) obj;

		if (this.getId().equals(tmpRaza.getId()) && this.getRaza().equals(tmpRaza.getRaza())) {
			if ((this.getObservacion() == null && tmpRaza.getObservacion() != null)
					|| (this.getObservacion() != null && tmpRaza.getObservacion() == null))
				return false;
			else if ((this.getObservacion() == null && tmpRaza.getObservacion() == null)
					|| (this.getObservacion().equals(tmpRaza.getObservacion())))
				return true;
		}
		return false;
	}
}