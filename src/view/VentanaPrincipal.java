package view;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import view.panel.administracion.PanelPerros;
import view.panel.administracion.PanelPersonal;
import view.panel.administracion.PanelSocios;
import view.panel.configuracion.PanelCargos;
import view.panel.configuracion.PanelEstadosPerro;
import view.panel.configuracion.PanelLocalizaciones;
import view.panel.configuracion.PanelRazasPerro;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;

public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = -6875864996453325107L;
	private String titulo;
	private JPanel panelBarrasDesplegables, panelTabs, panelToolBar, panelMenus;
	private JMenuBar menuPrincial;
	private JToolBar toolBar;
	private List<JButton> btnsAdministracion, btnsConfiguracion;
	private JButton tbPerros, tbPersonas;
	private TabModificada tab;
	private JPanel panelPerros, panelPersonal, panelSocios, panelLocalizaciones, panelCargos, panelRazas,
			panelEstadosPerros;
	private static JLabel estadoBDicon, estadoBD;
	private static Font fontEncabezado;

	public VentanaPrincipal(String nombreSistema, Integer version, Integer subversion) {
		this.titulo = nombreSistema + " " + version + "." + subversion;
		setTitle(titulo);

		cargarPanelMenus();
		cargarPanelToolBar();
		cargarPanelBarrasDesplegables();
		cargarPanelTabs();

		actionListeners();

		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void actionListeners() {
		btnsAdministracion.get(0).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Perros";
				if (!tab.existeTitulo(titulo)) {
					panelPerros = new PanelPerros();
					tab.addTab(titulo, new ImageIcon("icon/perro20x20.png"), panelPerros);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});
		tbPerros.addActionListener(btnsAdministracion.get(0).getActionListeners()[0]);

		btnsAdministracion.get(1).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Personal";
				if (!tab.existeTitulo(titulo)) {
					panelPersonal = new PanelPersonal();
					tab.addTab(titulo, new ImageIcon("icon/personas20x20.png"), panelPersonal);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		btnsAdministracion.get(2).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Socios";
				if (!tab.existeTitulo(titulo)) {
					panelSocios = new PanelSocios();
					tab.addTab(titulo, new ImageIcon("icon/socios20x20.png"), panelSocios);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		btnsConfiguracion.get(0).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Paises / Provincias / Ciudades";
				if (!tab.existeTitulo(titulo)) {
					panelLocalizaciones = new PanelLocalizaciones();
					tab.addTab(titulo, new ImageIcon("icon/map20x20.png"), panelLocalizaciones);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		btnsConfiguracion.get(1).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Cargos de personal";
				if (!tab.existeTitulo(titulo)) {
					panelCargos = new PanelCargos();
					tab.addTab(titulo, new ImageIcon("icon/maletin20x20.png"), panelCargos);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		btnsConfiguracion.get(2).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Razas";
				if (!tab.existeTitulo(titulo)) {
					panelRazas = new PanelRazasPerro();
					tab.addTab(titulo, new ImageIcon("icon/caniche20x20.png"), panelRazas);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});

		btnsConfiguracion.get(3).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String titulo = "Estados de perros";
				if (!tab.existeTitulo(titulo)) {
					panelEstadosPerros = new PanelEstadosPerro();
					tab.addTab(titulo, new ImageIcon("icon/estado20x20.png"), panelEstadosPerros);
					tab.setSelectedIndex(tab.getTabCount() - 1);
				} else
					JOptionPane.showMessageDialog(getParent(),
							"La pantalla \"" + titulo + "\" ya se encuentra abierta.", "Acci\u00f3n imposible",
							JOptionPane.WARNING_MESSAGE);
			}
		});
	}

	private void cargarPanelMenus() {
		getContentPane().setLayout(new GridBagLayout());
		panelMenus = new JPanel();
		panelMenus.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelMenus = new GridBagConstraints();
		gbc_panelMenus.gridx = 0;
		gbc_panelMenus.gridy = 0;
		gbc_panelMenus.weightx = 1.0;
		gbc_panelMenus.weighty = 0.0;
		gbc_panelMenus.gridheight = 1;
		gbc_panelMenus.gridwidth = 2;
		gbc_panelMenus.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelMenus.fill = GridBagConstraints.HORIZONTAL;
		getContentPane().add(panelMenus, gbc_panelMenus);

		menuPrincial = new JMenuBar();
		menuPrincial.add(new JMenu("Menu 1"));
		menuPrincial.add(new JMenu("Menu 2"));
		GridBagConstraints gbc_menuPrincial = new GridBagConstraints();
		gbc_menuPrincial.gridx = 0;
		gbc_menuPrincial.gridy = 0;
		gbc_menuPrincial.weightx = 1.0;
		gbc_menuPrincial.weighty = 1.0;
		gbc_menuPrincial.gridheight = 1;
		gbc_menuPrincial.gridwidth = 1;
		gbc_menuPrincial.anchor = GridBagConstraints.NORTHWEST;
		gbc_menuPrincial.fill = GridBagConstraints.BOTH;
		panelMenus.add(menuPrincial, gbc_menuPrincial);
	}

	private void cargarPanelToolBar() {
		panelToolBar = new JPanel();
		panelToolBar.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelToolBar = new GridBagConstraints();
		gbc_panelToolBar.gridx = 0;
		gbc_panelToolBar.gridy = 1;
		gbc_panelToolBar.weightx = 1.0;
		gbc_panelToolBar.weighty = 0.0;
		gbc_panelToolBar.gridheight = 1;
		gbc_panelToolBar.gridwidth = 2;
		gbc_panelToolBar.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelToolBar.fill = GridBagConstraints.HORIZONTAL;
		getContentPane().add(panelToolBar, gbc_panelToolBar);

		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		tbPerros = new JButton(new ImageIcon("icon/perro30x30.png"));
		tbPerros.setContentAreaFilled(false);
		toolBar.add(tbPerros);

		tbPersonas = new JButton(new ImageIcon("icon/personas30x30.png"));
		tbPersonas.setContentAreaFilled(false);
		toolBar.add(tbPersonas);

		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		gbc_toolBar.weightx = 1.0;
		gbc_toolBar.weighty = 1.0;
		gbc_toolBar.gridheight = 1;
		gbc_toolBar.gridwidth = 1;
		gbc_toolBar.anchor = GridBagConstraints.NORTHWEST;
		gbc_toolBar.fill = GridBagConstraints.BOTH;
		panelToolBar.add(toolBar, gbc_toolBar);
	}

	private void cargarPanelBarrasDesplegables() {
		panelBarrasDesplegables = new JPanel(new GridBagLayout());
		panelBarrasDesplegables.setBackground(new Color(122, 122, 122));
		GridBagConstraints gbc_panelBarrasDesplegables = new GridBagConstraints();
		gbc_panelBarrasDesplegables.gridx = 0;
		gbc_panelBarrasDesplegables.gridy = 2;
		gbc_panelBarrasDesplegables.weightx = 0.08;
		gbc_panelBarrasDesplegables.weighty = 1.0;
		gbc_panelBarrasDesplegables.gridheight = 1;
		gbc_panelBarrasDesplegables.gridwidth = 1;
		gbc_panelBarrasDesplegables.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelBarrasDesplegables.fill = GridBagConstraints.BOTH;
		getContentPane().add(panelBarrasDesplegables, gbc_panelBarrasDesplegables);

		btnsAdministracion = new ArrayList<JButton>();
		btnsAdministracion.add(new Boton("Perros", new ImageIcon("icon/perro20x20.png")));
		btnsAdministracion.add(new Boton("Personal", new ImageIcon("icon/personas20x20.png")));
		btnsAdministracion.add(new Boton("Socios", new ImageIcon("icon/socios20x20.png")));

		BarraDesplegable bdCAAN = new BarraDesplegable("Administraci\u00f3n", new ImageIcon("icon/caan.png"),
				btnsAdministracion, true);
		GridBagConstraints gbc_bdCAAN = new GridBagConstraints();
		gbc_bdCAAN.insets = new Insets(10, 10, 0, 10);
		gbc_bdCAAN.gridx = 0;
		gbc_bdCAAN.gridy = 0;
		gbc_bdCAAN.weightx = 1.0;
		gbc_bdCAAN.weighty = 0.0;
		gbc_bdCAAN.gridheight = 1;
		gbc_bdCAAN.gridwidth = 1;
		gbc_bdCAAN.anchor = GridBagConstraints.NORTHWEST;
		gbc_bdCAAN.fill = GridBagConstraints.HORIZONTAL;
		panelBarrasDesplegables.add(bdCAAN, gbc_bdCAAN);

		btnsConfiguracion = new ArrayList<JButton>();
		btnsConfiguracion.add(new Boton("Paises / Provincias / Ciudades", new ImageIcon("icon/map20x20.png")));
		btnsConfiguracion.add(new Boton("Cargos de personal", new ImageIcon("icon/maletin20x20.png")));
		btnsConfiguracion.add(new Boton("Razas", new ImageIcon("icon/caniche20x20.png")));
		btnsConfiguracion.add(new Boton("Estados de perros", new ImageIcon("icon/estado20x20.png")));

		BarraDesplegable bdConfiguracion = new BarraDesplegable("Configuraci\u00f3n",
				new ImageIcon("icon/configuracion.png"), btnsConfiguracion, false);
		GridBagConstraints gbc_bdConfiguracion = new GridBagConstraints();
		gbc_bdConfiguracion.insets = new Insets(10, 10, 0, 10);
		gbc_bdConfiguracion.gridx = 0;
		gbc_bdConfiguracion.gridy = 1;
		gbc_bdConfiguracion.weightx = 0.0;
		gbc_bdConfiguracion.weighty = 0.0;
		gbc_bdConfiguracion.gridheight = 1;
		gbc_bdConfiguracion.gridwidth = 1;
		gbc_bdConfiguracion.anchor = GridBagConstraints.NORTHWEST;
		gbc_bdConfiguracion.fill = GridBagConstraints.HORIZONTAL;

		fontEncabezado = new Font(bdConfiguracion.getEncabezado().getFont().getFamily(), Font.BOLD, 13);

		bdCAAN.getEncabezado().setFont(fontEncabezado);
		bdConfiguracion.getEncabezado().setFont(fontEncabezado);
		panelBarrasDesplegables.add(bdConfiguracion, gbc_bdConfiguracion);

		bdCAAN.setAlto(30);
		bdConfiguracion.setAlto(30);

		JPanel panelEstadoBD = new JPanel(new GridBagLayout());
		panelEstadoBD.setBackground(panelBarrasDesplegables.getBackground());
		GridBagConstraints gbc_panelEstadoBD = new GridBagConstraints();
		gbc_panelEstadoBD.gridx = 0;
		gbc_panelEstadoBD.gridy = 2;
		gbc_panelEstadoBD.weightx = 1.0;
		gbc_panelEstadoBD.weighty = 1.0;
		gbc_panelEstadoBD.gridheight = 1;
		gbc_panelEstadoBD.gridwidth = 1;
		gbc_panelEstadoBD.anchor = GridBagConstraints.SOUTHWEST;
		gbc_panelEstadoBD.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelEstadoBD.insets = new Insets(0, 0, 0, 0);
		panelBarrasDesplegables.add(panelEstadoBD, gbc_panelEstadoBD);

		JLabel lblBD = new JLabel("Conexi\u00f3n con la Base de Datos:");
		lblBD.setFont(fontEncabezado);
		GridBagConstraints gbc_lblBd = new GridBagConstraints();
		gbc_lblBd.gridx = 0;
		gbc_lblBd.gridy = 0;
		gbc_lblBd.weightx = 0.0;
		gbc_lblBd.weighty = 0.0;
		gbc_lblBd.gridheight = 2;
		gbc_lblBd.gridwidth = 1;
		gbc_lblBd.anchor = GridBagConstraints.SOUTHWEST;
		gbc_lblBd.fill = GridBagConstraints.NONE;
		gbc_lblBd.insets = new Insets(0, 10, 30, 0);
		panelEstadoBD.add(lblBD, gbc_lblBd);

		estadoBDicon = new JLabel();
		GridBagConstraints gbc_estadoBDicon = new GridBagConstraints();
		gbc_estadoBDicon.gridx = 1;
		gbc_estadoBDicon.gridy = 0;
		gbc_estadoBDicon.weightx = 0.0;
		gbc_estadoBDicon.weighty = 1.0;
		gbc_estadoBDicon.gridheight = 1;
		gbc_estadoBDicon.gridwidth = 1;
		gbc_estadoBDicon.anchor = GridBagConstraints.SOUTH;
		gbc_estadoBDicon.fill = GridBagConstraints.NONE;
		gbc_estadoBDicon.insets = new Insets(0, 10, 0, 0);
		panelEstadoBD.add(estadoBDicon, gbc_estadoBDicon);

		estadoBD = new JLabel();
		GridBagConstraints gbc_estadoBD = new GridBagConstraints();
		gbc_estadoBD.gridx = 1;
		gbc_estadoBD.gridy = 1;
		gbc_estadoBD.weightx = 1.0;
		gbc_estadoBD.weighty = 1.0;
		gbc_estadoBD.gridheight = 1;
		gbc_estadoBD.gridwidth = 1;
		gbc_estadoBD.anchor = GridBagConstraints.SOUTH;
		gbc_estadoBD.fill = GridBagConstraints.NONE;
		gbc_estadoBD.insets = new Insets(0, 10, 20, 0);
		panelEstadoBD.add(estadoBD, gbc_estadoBD);

		panelBarrasDesplegables.setPreferredSize(panelBarrasDesplegables.getPreferredSize());
	}

	private void cargarPanelTabs() {
		panelTabs = new JPanel();
		panelTabs.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelTabs = new GridBagConstraints();
		gbc_panelTabs.gridx = 1;
		gbc_panelTabs.gridy = 2;
		gbc_panelTabs.weightx = 0.92;
		gbc_panelTabs.weighty = 1.0;
		gbc_panelTabs.gridheight = 1;
		gbc_panelTabs.gridwidth = 1;
		gbc_panelTabs.anchor = GridBagConstraints.NORTHWEST;
		gbc_panelTabs.fill = GridBagConstraints.BOTH;
		getContentPane().add(panelTabs, gbc_panelTabs);

		tab = new TabModificada();
		GridBagConstraints gbc_tab = new GridBagConstraints();
		gbc_tab.gridx = 0;
		gbc_tab.gridy = 0;
		gbc_tab.weightx = 1.0;
		gbc_tab.weighty = 1.0;
		gbc_tab.gridheight = 1;
		gbc_tab.gridwidth = 1;
		gbc_tab.anchor = GridBagConstraints.CENTER;
		gbc_tab.fill = GridBagConstraints.BOTH;
		panelTabs.add(tab, gbc_tab);
	}

	public static void actualizarConexion(int conexion) {
		if (conexion == 0) {
			estadoBDicon.setIcon(new ImageIcon("icon/bdalerta30x30.png"));
			estadoBD.setForeground(Color.YELLOW);
			estadoBD.setText("Conectando...");
		} else if (conexion == 1) {
			estadoBDicon.setIcon(new ImageIcon("icon/bdcorrecto30x30.png"));
			estadoBD.setForeground(new Color(48, 211, 167));
			estadoBD.setText("Conexi\u00f3n exitosa.");
		} else if (conexion == -1) {
			estadoBDicon.setIcon(new ImageIcon("icon/bderror30x30.png"));
			estadoBD.setForeground(new Color(215, 40, 40));
			estadoBD.setText("Error de conexi\u00f3n.");
		}
		estadoBD.setFont(fontEncabezado);
	}
}