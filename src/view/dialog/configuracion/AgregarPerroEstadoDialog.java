package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import controller.PerroEstadoController;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import model.PerroEstado;
import view.panel.configuracion.PanelEstadosPerro;
import java.awt.BorderLayout;

public class AgregarPerroEstadoDialog extends JDialog {

	private static final long serialVersionUID = 8447831717256433299L;
	private JPanel panel;
	private JTextField tfObservaciones, tfDescripcion;
	private JCheckBox chkActivo;
	private JButton btnGrabar, btnCancelar;
	private PanelEstadosPerro parentPanel;
	private JDialog dialogo;
	private JLabel lblPerroEnPredio;
	private JCheckBox chkPerroEnPredio;

	public AgregarPerroEstadoDialog(JFrame f_ppal, PanelEstadosPerro parentPanel) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		setTitle("Grabar estado");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		chkActivo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!chkActivo.isSelected()) {
					chkPerroEnPredio.setSelected(false);
					chkPerroEnPredio.setEnabled(false);
				} else {
					chkPerroEnPredio.setEnabled(true);
				}
			}
		});
		
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarEstado())
					dispose();
			}

			private boolean grabarEstado() {
				if (tfDescripcion.getText() == null || tfDescripcion.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo, "El campo estado no puede quedar vac\u00edo.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

				PerroEstado estado = new PerroEstado(tfDescripcion.getText(), chkActivo.isSelected(),chkPerroEnPredio.isSelected());

				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					estado.setObservacion(tfObservaciones.getText());

				PerroEstadoController perroEstadoController = PerroEstadoController.getInstance();
				if (perroEstadoController.agregar(estado)) {
					JOptionPane.showMessageDialog(dialogo,
							"El estado \"" + tfDescripcion.getText() + "\" fue grabada con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					parentPanel.actualizar();
					return true;
				} else {
					JOptionPane.showMessageDialog(dialogo,
							"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfDescripcion.getText()
									+ "\" no se encontraba previamente ingresado.",
							"Error", JOptionPane.WARNING_MESSAGE);
					return false;
				}
			}

		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		JLabel lblDescripcion = new JLabel("Descripci\u00f3n");
		GridBagConstraints gbc_lblDescripcion = new GridBagConstraints();
		gbc_lblDescripcion.gridx = 0;
		gbc_lblDescripcion.gridy = 0;
		gbc_lblDescripcion.gridwidth = 1;
		gbc_lblDescripcion.gridheight = 1;
		gbc_lblDescripcion.weightx = 0.0;
		gbc_lblDescripcion.weighty = 0.0;
		gbc_lblDescripcion.fill = GridBagConstraints.BOTH;
		gbc_lblDescripcion.insets = new Insets(15, 15, 10, 15);
		panel.add(lblDescripcion, gbc_lblDescripcion);

		tfDescripcion = new JTextField();
		GridBagConstraints gbc_tfDescripcion = new GridBagConstraints();
		gbc_tfDescripcion.gridx = 1;
		gbc_tfDescripcion.gridy = 0;
		gbc_tfDescripcion.gridwidth = 1;
		gbc_tfDescripcion.gridheight = 1;
		gbc_tfDescripcion.weightx = 1.0;
		gbc_tfDescripcion.weighty = 0.0;
		gbc_tfDescripcion.fill = GridBagConstraints.BOTH;
		gbc_tfDescripcion.insets = new Insets(15, 0, 10, 15);
		panel.add(tfDescripcion, gbc_tfDescripcion);

		JLabel lblActivo = new JLabel("Activo");
		GridBagConstraints gbc_lblActivo = new GridBagConstraints();
		gbc_lblActivo.gridx = 0;
		gbc_lblActivo.gridy = 1;
		gbc_lblActivo.gridwidth = 1;
		gbc_lblActivo.gridheight = 1;
		gbc_lblActivo.weightx = 0.0;
		gbc_lblActivo.weighty = 0.0;
		gbc_lblActivo.fill = GridBagConstraints.BOTH;
		gbc_lblActivo.insets = new Insets(0, 15, 10, 15);
		panel.add(lblActivo, gbc_lblActivo);

		chkActivo = new JCheckBox();
		chkActivo.setSelected(true);
		GridBagConstraints gbc_chkActivo = new GridBagConstraints();
		gbc_chkActivo.gridx = 1;
		gbc_chkActivo.gridy = 1;
		gbc_chkActivo.gridwidth = 1;
		gbc_chkActivo.gridheight = 1;
		gbc_chkActivo.weightx = 1.0;
		gbc_chkActivo.weighty = 0.0;
		gbc_chkActivo.fill = GridBagConstraints.NONE;
		gbc_chkActivo.insets = new Insets(0, 0, 10, 15);
		panel.add(chkActivo, gbc_chkActivo);
		
		lblPerroEnPredio = new JLabel("Perro en predio");
		GridBagConstraints gbc_lblPerroEnPredio = new GridBagConstraints();
		gbc_lblPerroEnPredio.gridx = 0;
		gbc_lblPerroEnPredio.gridy = 2;
		gbc_lblPerroEnPredio.gridwidth = 1;
		gbc_lblPerroEnPredio.gridheight = 1;
		gbc_lblPerroEnPredio.weightx = 0.0;
		gbc_lblPerroEnPredio.weighty = 0.0;
		gbc_lblPerroEnPredio.fill = GridBagConstraints.BOTH;
		gbc_lblPerroEnPredio.insets = new Insets(0, 15, 10, 15);
		panel.add(lblPerroEnPredio, gbc_lblPerroEnPredio);
		
		chkPerroEnPredio = new JCheckBox();
		chkPerroEnPredio.setSelected(true);
		GridBagConstraints gbc_chkPerroEnPredio = new GridBagConstraints();
		gbc_chkPerroEnPredio.gridx = 1;
		gbc_chkPerroEnPredio.gridy = 2;
		gbc_chkPerroEnPredio.gridwidth = 1;
		gbc_chkPerroEnPredio.gridheight = 1;
		gbc_chkPerroEnPredio.weightx = 1.0;
		gbc_chkPerroEnPredio.weighty = 0.0;
		gbc_chkPerroEnPredio.fill = GridBagConstraints.NONE;
		gbc_chkPerroEnPredio.insets = new Insets(0, 0, 10, 15);
		panel.add(chkPerroEnPredio, gbc_chkPerroEnPredio);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 3;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.0;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 3;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 1.0;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 4;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 60, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 4;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}