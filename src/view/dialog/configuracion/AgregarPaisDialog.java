package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import controller.PaisController;
import model.Pais;
import view.panel.configuracion.PanelLocalizaciones;
import javax.swing.JButton;

public class AgregarPaisDialog extends JDialog {

	private static final long serialVersionUID = -7655540420154613888L;
	private JPanel panel;
	private JTextField tfLocalizacion,tfObservaciones;
	private JButton btnGrabar,btnCancelar;
	private JDialog dialogo;
	private PanelLocalizaciones pl;

	public AgregarPaisDialog(JFrame f_ppal, PanelLocalizaciones pl) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.pl = pl;
		setTitle("Grabar pa\u00EDs");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarPais()) {
					JOptionPane.showMessageDialog(dialogo,
							"El pa\u00eds \"" + tfLocalizacion.getText() + "\" fue grabado con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					pl.actualizar();
					dispose();
				}
			}

			private boolean grabarPais() {
				if (tfLocalizacion.getText() != null && !tfLocalizacion.getText().equals("")) {
					Pais pais = new Pais(tfLocalizacion.getText());
					if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
						pais.setObservacion(tfObservaciones.getText());

					PaisController paisController = PaisController.getInstance();
					if (paisController.agregar(pais))
						return true;
					else
						JOptionPane.showMessageDialog(dialogo,
								"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfLocalizacion.getText()
										+ "\" no se encontraba previamente ingresado.",
								"Error", JOptionPane.WARNING_MESSAGE);
				} else
					JOptionPane.showMessageDialog(dialogo, "No ingres\u00f3 ning\u00fan pa\u00eds.", "Error",
							JOptionPane.ERROR_MESSAGE);
				return false;
			}

		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		JLabel lblPais = new JLabel("Pa\u00EDs");
		GridBagConstraints gbc_lblPais = new GridBagConstraints();
		gbc_lblPais.gridx = 0;
		gbc_lblPais.gridy = 0;
		gbc_lblPais.gridwidth = 1;
		gbc_lblPais.gridheight = 1;
		gbc_lblPais.weightx = 0.5;
		gbc_lblPais.weighty = 0.0;
		gbc_lblPais.fill = GridBagConstraints.BOTH;
		gbc_lblPais.insets = new Insets(15, 15, 10, 15);
		panel.add(lblPais, gbc_lblPais);

		tfLocalizacion = new JTextField();
		GridBagConstraints gbc_tfLocalizacion = new GridBagConstraints();
		gbc_tfLocalizacion.gridx = 1;
		gbc_tfLocalizacion.gridy = 0;
		gbc_tfLocalizacion.gridwidth = 1;
		gbc_tfLocalizacion.gridheight = 1;
		gbc_tfLocalizacion.weightx = 0.5;
		gbc_tfLocalizacion.weighty = 0.0;
		gbc_tfLocalizacion.fill = GridBagConstraints.BOTH;
		gbc_tfLocalizacion.insets = new Insets(15, 0, 10, 15);
		panel.add(tfLocalizacion, gbc_tfLocalizacion);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 1;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.5;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 1;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 0.5;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 2;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 40, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");

		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 2;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);
		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}