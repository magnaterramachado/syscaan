package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import controller.PerroRazaController;
import model.PerroRaza;
import view.panel.configuracion.PanelRazasPerro;
import java.awt.BorderLayout;

public class AgregarPerroRazaDialog extends JDialog {

	private static final long serialVersionUID = 8447831717256433299L;
	private JPanel panel;
	private JTextField tfObservaciones, tfRaza;
	private JButton btnGrabar, btnCancelar;
	private PanelRazasPerro parentPanel;
	private JDialog dialogo;

	public AgregarPerroRazaDialog(JFrame f_ppal,PanelRazasPerro parentPanel) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		setTitle("Grabar raza");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarRaza())
					dispose();
			}

			private boolean grabarRaza() {
				if (tfRaza.getText() == null || tfRaza.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo,
							"El campo raza no puede quedar vac\u00edo.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}
				
				PerroRaza raza = new PerroRaza(tfRaza.getText());

				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					raza.setObservacion(tfObservaciones.getText());

				PerroRazaController razaController = PerroRazaController.getInstance();
				if (razaController.agregar(raza)) {
					JOptionPane.showMessageDialog(dialogo,
							"La raza \"" + tfRaza.getText() + "\" fue grabada con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					parentPanel.actualizar();
					return true;
				} else {
					JOptionPane.showMessageDialog(dialogo,
							"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfRaza.getText()
									+ "\" no se encontraba previamente ingresado.",
							"Error", JOptionPane.WARNING_MESSAGE);
					return false;
				}
			}

		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		JLabel lblRaza = new JLabel("Raza");
		GridBagConstraints gbc_lblRaza = new GridBagConstraints();
		gbc_lblRaza.gridx = 0;
		gbc_lblRaza.gridy = 0;
		gbc_lblRaza.gridwidth = 1;
		gbc_lblRaza.gridheight = 1;
		gbc_lblRaza.weightx = 0.0;
		gbc_lblRaza.weighty = 0.0;
		gbc_lblRaza.fill = GridBagConstraints.BOTH;
		gbc_lblRaza.insets = new Insets(15, 15, 10, 15);
		panel.add(lblRaza, gbc_lblRaza);

		tfRaza = new JTextField();
		GridBagConstraints gbc_tfRaza = new GridBagConstraints();
		gbc_tfRaza.gridx = 1;
		gbc_tfRaza.gridy = 0;
		gbc_tfRaza.gridwidth = 1;
		gbc_tfRaza.gridheight = 1;
		gbc_tfRaza.weightx = 1.0;
		gbc_tfRaza.weighty = 0.0;
		gbc_tfRaza.fill = GridBagConstraints.BOTH;
		gbc_tfRaza.insets = new Insets(15, 0, 10, 15);
		panel.add(tfRaza, gbc_tfRaza);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 1;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.0;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 1;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 1.0;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 2;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 60, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 2;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}