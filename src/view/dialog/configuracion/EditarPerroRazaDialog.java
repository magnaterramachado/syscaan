package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import controller.PerroRazaController;
import model.PerroRaza;
import view.panel.configuracion.PanelRazasPerro;
import java.awt.BorderLayout;

public class EditarPerroRazaDialog extends JDialog {

	private static final long serialVersionUID = 8447831717256433299L;
	private PerroRaza raza;
	private JPanel panel;
	private JTextField tfId, tfObservaciones, tfRaza;
	private JButton btnGrabar, btnCancelar;
	private PanelRazasPerro parentPanel;
	private JDialog dialogo;
	private JLabel lblId;

	public EditarPerroRazaDialog(JFrame f_ppal, PanelRazasPerro parentPanel, PerroRaza raza) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.raza = raza;
		this.dialogo = this;
		this.parentPanel = parentPanel;
		setTitle("Editar raza");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (editarRaza())
					dispose();
			}

			private boolean controlarModificaciones() {

				PerroRaza raza2 = new PerroRaza(tfRaza.getText());
				raza2.setId(raza.getId());
				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					raza2.setObservacion(tfObservaciones.getText());
				
				if (raza.equals(raza2)) {
					JOptionPane.showMessageDialog(dialogo, "No realiz\u00f3 ninguna modificaci\u00f3n.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				} else
					return true;
			}

			private boolean editarRaza() {
				if (controlarModificaciones()) {

					if (tfRaza.getText() == null || tfRaza.getText().equals("")) {
						JOptionPane.showMessageDialog(dialogo, "El campo raza no puede quedar vac\u00edo.", "Error",
								JOptionPane.ERROR_MESSAGE);
						return false;
					}
					raza.setRaza(tfRaza.getText());

					if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
						raza.setObservacion(tfObservaciones.getText());
					else
						raza.setObservacion(null);

					PerroRazaController razaController = PerroRazaController.getInstance();
					razaController.modificar(raza);
						JOptionPane.showMessageDialog(dialogo,
								"La raza \"" + tfRaza.getText() + "\" fue modificada con \u00e9xito.", "Operaci\u00f3n concreatada",
								JOptionPane.INFORMATION_MESSAGE);
						parentPanel.actualizar();
						return true;
				} else
					return false;
			}
		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		lblId = new JLabel("Id");
		GridBagConstraints gbc_lblId = new GridBagConstraints();
		gbc_lblId.gridx = 0;
		gbc_lblId.gridy = 0;
		gbc_lblId.gridwidth = 1;
		gbc_lblId.gridheight = 1;
		gbc_lblId.weightx = 0.0;
		gbc_lblId.weighty = 0.0;
		gbc_lblId.fill = GridBagConstraints.BOTH;
		gbc_lblId.insets = new Insets(15, 15, 10, 15);
		panel.add(lblId, gbc_lblId);

		tfId = new JTextField(raza.getId().toString());
		tfId.setEnabled(false);
		GridBagConstraints gbc_tfId = new GridBagConstraints();
		gbc_tfId.gridx = 1;
		gbc_tfId.gridy = 0;
		gbc_tfId.gridwidth = 1;
		gbc_tfId.gridheight = 1;
		gbc_tfId.weightx = 1.0;
		gbc_tfId.weighty = 0.0;
		gbc_tfId.fill = GridBagConstraints.BOTH;
		gbc_tfId.insets = new Insets(15, 0, 10, 15);
		panel.add(tfId, gbc_tfId);

		JLabel lblRaza = new JLabel("Raza");
		GridBagConstraints gbc_lblRaza = new GridBagConstraints();
		gbc_lblRaza.gridx = 0;
		gbc_lblRaza.gridy = 1;
		gbc_lblRaza.gridwidth = 1;
		gbc_lblRaza.gridheight = 1;
		gbc_lblRaza.weightx = 0.0;
		gbc_lblRaza.weighty = 0.0;
		gbc_lblRaza.fill = GridBagConstraints.BOTH;
		gbc_lblRaza.insets = new Insets(0, 15, 10, 15);
		panel.add(lblRaza, gbc_lblRaza);

		tfRaza = new JTextField(raza.getRaza());
		GridBagConstraints gbc_tfRaza = new GridBagConstraints();
		gbc_tfRaza.gridx = 1;
		gbc_tfRaza.gridy = 1;
		gbc_tfRaza.gridwidth = 1;
		gbc_tfRaza.gridheight = 1;
		gbc_tfRaza.weightx = 1.0;
		gbc_tfRaza.weighty = 0.0;
		gbc_tfRaza.fill = GridBagConstraints.BOTH;
		gbc_tfRaza.insets = new Insets(0, 0, 10, 15);
		panel.add(tfRaza, gbc_tfRaza);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 2;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.0;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		if (raza.getObservacion() != null)
			tfObservaciones.setText(raza.getObservacion());
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 2;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 1.0;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 3;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 60, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 3;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}