package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import controller.PersonalCargoController;
import model.PersonalCargo;
import view.panel.configuracion.PanelCargos;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.Dimension;
import javax.swing.JTextArea;

public class AgregarPersonalCargoDialog extends JDialog {

	private static final long serialVersionUID = 8447831717256433299L;
	private JPanel panel;
	private JTextField tfObservaciones, tfCargo;
	private JCheckBox chkComisionDirectiva;
	private JButton btnGrabar, btnCancelar;
	private PanelCargos parentPanel;
	private JDialog dialogo;
	private JTextArea taTareas;

	public AgregarPersonalCargoDialog(JFrame f_ppal, PanelCargos parentPanel) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		setTitle("Grabar cargo de personal");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() / 2, f_ppal.getHeight() / 8);
		pack();
		setPreferredSize(new Dimension(getWidth(), getHeight() + 100));
		setSize(new Dimension(getWidth(), getHeight() + 100));
		setResizable(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarCargo())
					dispose();
			}

			private boolean grabarCargo() {
				if (tfCargo.getText() == null || tfCargo.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Nombre del cargo\" no puede quedar vac\u00edo.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}

				PersonalCargo cargo = new PersonalCargo(tfCargo.getText(), chkComisionDirectiva.isSelected());

				if (taTareas.getText() != null && !taTareas.getText().equals(""))
					cargo.setDescripcionTareas(taTareas.getText());

				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					cargo.setObservacion(tfObservaciones.getText());

				PersonalCargoController personalCargoController = PersonalCargoController.getInstance();
				if (personalCargoController.agregar(cargo)) {
					JOptionPane.showMessageDialog(dialogo,
							"El cargo \"" + tfCargo.getText() + "\" fue grabado con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					parentPanel.actualizar();
					return true;
				} else {
					JOptionPane.showMessageDialog(dialogo,
							"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfCargo.getText()
									+ "\" no se encontraba previamente ingresado.",
							"Error", JOptionPane.WARNING_MESSAGE);
					return false;
				}
			}
		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		JLabel lblCargo = new JLabel("Nombre del cargo");
		GridBagConstraints gbc_lblCargo = new GridBagConstraints();
		gbc_lblCargo.gridx = 0;
		gbc_lblCargo.gridy = 0;
		gbc_lblCargo.gridwidth = 1;
		gbc_lblCargo.gridheight = 1;
		gbc_lblCargo.weightx = 0.0;
		gbc_lblCargo.weighty = 0.0;
		gbc_lblCargo.fill = GridBagConstraints.BOTH;
		gbc_lblCargo.insets = new Insets(15, 15, 10, 15);
		panel.add(lblCargo, gbc_lblCargo);

		tfCargo = new JTextField();
		GridBagConstraints gbc_tfCargo = new GridBagConstraints();
		gbc_tfCargo.gridx = 1;
		gbc_tfCargo.gridy = 0;
		gbc_tfCargo.gridwidth = 2;
		gbc_tfCargo.gridheight = 1;
		gbc_tfCargo.weightx = 1.0;
		gbc_tfCargo.weighty = 0.0;
		gbc_tfCargo.fill = GridBagConstraints.BOTH;
		gbc_tfCargo.insets = new Insets(15, 0, 10, 15);
		panel.add(tfCargo, gbc_tfCargo);

		JLabel lblTareas = new JLabel("Descripci\u00F3n de tareas");
		GridBagConstraints gbc_lblTareas = new GridBagConstraints();
		gbc_lblTareas.gridx = 0;
		gbc_lblTareas.gridy = 1;
		gbc_lblTareas.gridwidth = 1;
		gbc_lblTareas.gridheight = 1;
		gbc_lblTareas.weightx = 0.0;
		gbc_lblTareas.weighty = 1.0;
		gbc_lblTareas.fill = GridBagConstraints.BOTH;
		gbc_lblTareas.insets = new Insets(0, 15, 10, 15);
		panel.add(lblTareas, gbc_lblTareas);

		taTareas = new JTextArea();
		JScrollPane scroll = new JScrollPane(taTareas);
		GridBagConstraints gbc_taTareas = new GridBagConstraints();
		gbc_taTareas.gridx = 1;
		gbc_taTareas.gridy = 1;
		gbc_taTareas.gridwidth = 2;
		gbc_taTareas.gridheight = 1;
		gbc_taTareas.weightx = 1.0;
		gbc_taTareas.weighty = 1.0;
		gbc_taTareas.fill = GridBagConstraints.BOTH;
		gbc_taTareas.insets = new Insets(0, 0, 10, 15);
		panel.add(scroll, gbc_taTareas);

		JLabel lblComisionDirectiva = new JLabel("Comisi\u00F3n directiva");
		GridBagConstraints gbc_lblComisionDirectiva = new GridBagConstraints();
		gbc_lblComisionDirectiva.gridx = 0;
		gbc_lblComisionDirectiva.gridy = 2;
		gbc_lblComisionDirectiva.gridwidth = 1;
		gbc_lblComisionDirectiva.gridheight = 1;
		gbc_lblComisionDirectiva.weightx = 0.0;
		gbc_lblComisionDirectiva.weighty = 0.0;
		gbc_lblComisionDirectiva.fill = GridBagConstraints.BOTH;
		gbc_lblComisionDirectiva.insets = new Insets(0, 15, 10, 15);
		panel.add(lblComisionDirectiva, gbc_lblComisionDirectiva);

		chkComisionDirectiva = new JCheckBox();
		GridBagConstraints gbc_chkComisionDirectiva = new GridBagConstraints();
		gbc_chkComisionDirectiva.gridx = 1;
		gbc_chkComisionDirectiva.gridy = 2;
		gbc_chkComisionDirectiva.gridwidth = 1;
		gbc_chkComisionDirectiva.gridheight = 1;
		gbc_chkComisionDirectiva.weightx = 1.0;
		gbc_chkComisionDirectiva.weighty = 0.0;
		gbc_chkComisionDirectiva.anchor = GridBagConstraints.WEST;
		gbc_chkComisionDirectiva.fill = GridBagConstraints.NONE;
		gbc_chkComisionDirectiva.insets = new Insets(0, -4, 10, 15);
		panel.add(chkComisionDirectiva, gbc_chkComisionDirectiva);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 3;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.0;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 3;
		gbc_tfObservaciones.gridwidth = 2;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 1.0;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		JPanel panelBotones = new JPanel();
		panelBotones.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelBotones = new GridBagConstraints();
		gbc_panelBotones.gridx = 1;
		gbc_panelBotones.gridy = 4;
		gbc_panelBotones.gridwidth = 2;
		gbc_panelBotones.gridheight = 1;
		gbc_panelBotones.weightx = 0.0;
		gbc_panelBotones.weighty = 0.0;
		gbc_panelBotones.anchor = GridBagConstraints.EAST;
		gbc_panelBotones.fill = GridBagConstraints.BOTH;
		gbc_panelBotones.insets = new Insets(0, 0, 0, 15);
		panel.add(panelBotones, gbc_panelBotones);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 0;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 1.0;
		gbc_btnGrabar.weighty = 0.0;
		gbc_btnGrabar.anchor = GridBagConstraints.EAST;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(0, 0, 15, 15);
		panelBotones.add(btnGrabar, gbc_btnGrabar);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 0;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.0;
		gbc_btnCancelar.weighty = 0.0;
		gbc_btnCancelar.anchor = GridBagConstraints.EAST;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(0, 0, 15, 0);
		panelBotones.add(btnCancelar, gbc_btnCancelar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}