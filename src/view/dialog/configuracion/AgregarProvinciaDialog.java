package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import controller.ProvinciaController;
import model.Pais;
import model.Provincia;
import view.panel.configuracion.PanelLocalizaciones;

public class AgregarProvinciaDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTextField tfLocalizacion, tfObservaciones;
	private JButton btnGrabar, btnCancelar;
	private JDialog dialogo;
	private PanelLocalizaciones parentPanel;
	private JComboBox<Pais> cbPaises;
	private List<Pais> paises;
	private int posPaisSeleccionado;

	public AgregarProvinciaDialog(JFrame f_ppal, PanelLocalizaciones parentPanel, List<Pais> paises,
			int posPaisSeleccionado) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		this.paises = paises;
		this.posPaisSeleccionado = posPaisSeleccionado;
		setTitle("Grabar provincia");
		setBackground(SystemColor.window);

		this.panel = new JPanel(new GridBagLayout());
		add(panel);
		cargarComponentes();
		actionListeners();
		
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
		tfLocalizacion.requestFocus();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarProvincia())
					dispose();
			}

			private boolean grabarProvincia() {
				if (tfLocalizacion.getText() != null && !tfLocalizacion.getText().equals("")) {
					Provincia provincia = new Provincia((Pais) cbPaises.getSelectedItem(), tfLocalizacion.getText());
					if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
						provincia.setObservacion(tfObservaciones.getText());

					ProvinciaController provinciaController = ProvinciaController.getInstance();
					if (provinciaController.agregar(provincia)) {
						JOptionPane.showMessageDialog(dialogo,
								"La provincia \"" + tfLocalizacion.getText() + "\" fue grabado con \u00e9xito.",
								"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
						parentPanel.actualizar();
						return true;
					} else
						JOptionPane.showMessageDialog(dialogo,
								"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfLocalizacion.getText()
										+ "\" no se encontraba previamente ingresado.",
								"Error", JOptionPane.WARNING_MESSAGE);
				} else
					JOptionPane.showMessageDialog(dialogo, "No ingres\u00f3 ninguna provincia.", "Error",
							JOptionPane.ERROR_MESSAGE);
				return false;
			}
		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		JLabel lblPais = new JLabel("Pa\u00EDs");
		GridBagConstraints gbc_lblPais = new GridBagConstraints();
		gbc_lblPais.gridx = 0;
		gbc_lblPais.gridy = 0;
		gbc_lblPais.gridwidth = 1;
		gbc_lblPais.gridheight = 1;
		gbc_lblPais.weightx = 0.5;
		gbc_lblPais.weighty = 0.0;
		gbc_lblPais.fill = GridBagConstraints.BOTH;
		gbc_lblPais.insets = new Insets(15, 15, 10, 15);
		panel.add(lblPais, gbc_lblPais);

		cbPaises = new JComboBox<Pais>();

		for (int i = 0; i < paises.size(); i++)
			cbPaises.addItem(paises.get(i));
		cbPaises.setSelectedIndex(posPaisSeleccionado);
		GridBagConstraints gbc_cbPaises = new GridBagConstraints();
		gbc_cbPaises.gridx = 1;
		gbc_cbPaises.gridy = 0;
		gbc_cbPaises.gridwidth = 1;
		gbc_cbPaises.gridheight = 1;
		gbc_cbPaises.weightx = 0.5;
		gbc_cbPaises.weighty = 0.0;
		gbc_cbPaises.fill = GridBagConstraints.BOTH;
		gbc_cbPaises.insets = new Insets(15, 0, 15, 15);
		panel.add(cbPaises, gbc_cbPaises);

		JLabel lblProvincia = new JLabel("Provincia");
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.gridx = 0;
		gbc_lblProvincia.gridy = 1;
		gbc_lblProvincia.gridwidth = 1;
		gbc_lblProvincia.gridheight = 1;
		gbc_lblProvincia.weightx = 0.5;
		gbc_lblProvincia.weighty = 0.0;
		gbc_lblProvincia.fill = GridBagConstraints.BOTH;
		gbc_lblProvincia.insets = new Insets(10, 15, 10, 15);
		panel.add(lblProvincia, gbc_lblProvincia);

		tfLocalizacion = new JTextField();
		GridBagConstraints gbc_tfLocalizacion = new GridBagConstraints();
		gbc_tfLocalizacion.gridx = 1;
		gbc_tfLocalizacion.gridy = 1;
		gbc_tfLocalizacion.gridwidth = 1;
		gbc_tfLocalizacion.gridheight = 1;
		gbc_tfLocalizacion.weightx = 0.5;
		gbc_tfLocalizacion.weighty = 0.0;
		gbc_tfLocalizacion.fill = GridBagConstraints.BOTH;
		gbc_tfLocalizacion.insets = new Insets(10, 0, 10, 15);
		panel.add(tfLocalizacion, gbc_tfLocalizacion);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 2;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.5;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 2;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 0.5;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 3;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 40, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 3;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);
		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}