package view.dialog.configuracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import admin.SistemaCAAN;
import javax.swing.JButton;
import javax.swing.JComboBox;
import controller.LocalidadController;
import controller.ProvinciaController;
import model.Localidad;
import model.Pais;
import model.Provincia;
import view.panel.configuracion.PanelLocalizaciones;
import java.awt.BorderLayout;

public class AgregarLocalidadDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private ProvinciaController provinciaController;
	private JTextField tfLocalizacion, tfObservaciones, tfCodPostal, tfPrefTelefonico;
	private JButton btnGrabar, btnCancelar;
	private JDialog dialogo;
	private PanelLocalizaciones parentPanel;
	private JComboBox<Pais> cbPaises;
	private JComboBox<Provincia> cbProvincias;
	private List<Pais> paises;
	private List<Provincia> provincias;
	private int posPaisSeleccionado, posProvinciaSeleccionada;

	public AgregarLocalidadDialog(JFrame f_ppal, PanelLocalizaciones parentPanel, List<Pais> paises,
			int posPaisSeleccionado, List<Provincia> provincias, int posProvinciaSeleccionada) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		this.paises = paises;
		this.posPaisSeleccionado = posPaisSeleccionado;
		this.provincias = provincias;
		this.posProvinciaSeleccionada = posProvinciaSeleccionada;
		provinciaController = ProvinciaController.getInstance();
		setTitle("Grabar localidad");
		setBackground(SystemColor.window);

		this.panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		cargarComponentes();
		actionListeners();
		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() * 2 / 3, f_ppal.getHeight() / 8);
		pack();
//		setSize(getWidth()+100,getHeight());
		tfLocalizacion.requestFocus();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarLocalidad())
					dispose();
			}

			private boolean grabarLocalidad() {
				if (tfLocalizacion.getText() == null || tfLocalizacion.getText().equals("")
						|| tfPrefTelefonico.getText() == null || tfPrefTelefonico.getText().equals("")
						|| tfCodPostal.getText() == null || tfCodPostal.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo,
							"La localidad, el c\u00f3digo postal y el prefijo telef\u00f3nico no pueden contener valores nulos.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (SistemaCAAN.convertInt(tfCodPostal.getText()) == null) {
					JOptionPane.showMessageDialog(panel, "El C\u00f3digo Postal no puede contener valores no num\u00e9rico.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (SistemaCAAN.convertInt(tfPrefTelefonico.getText()) == null) {
					JOptionPane.showMessageDialog(panel, "El Prefijo Telef\u00f3nico no puede contener valores no num\u00e9rico.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}

				String prefijoTel = SistemaCAAN.convertInt(tfPrefTelefonico.getText()).toString();
				String codPostal = SistemaCAAN.convertInt(tfCodPostal.getText()).toString();

				Localidad localidad = new Localidad((Provincia) cbProvincias.getSelectedItem(),
						tfLocalizacion.getText(), codPostal, prefijoTel);
				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					localidad.setObservacion(tfObservaciones.getText());

				LocalidadController localidadController = LocalidadController.getInstance();
				if (localidadController.agregar(localidad)) {
					JOptionPane.showMessageDialog(dialogo,
							"La localidad \"" + tfLocalizacion.getText() + "\" fue grabada con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					parentPanel.actualizar();
					return true;
				} else {
					JOptionPane.showMessageDialog(dialogo,
							"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfLocalizacion.getText()
									+ "\" no se encontraba previamente ingresado.",
							"Error", JOptionPane.WARNING_MESSAGE);
					return false;
				}
			}

		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		JLabel lblPais = new JLabel("Pa\u00EDs");
		GridBagConstraints gbc_lblPais = new GridBagConstraints();
		gbc_lblPais.gridx = 0;
		gbc_lblPais.gridy = 0;
		gbc_lblPais.gridwidth = 1;
		gbc_lblPais.gridheight = 1;
		gbc_lblPais.weightx = 0.5;
		gbc_lblPais.weighty = 0.0;
		gbc_lblPais.fill = GridBagConstraints.BOTH;
		gbc_lblPais.insets = new Insets(15, 15, 10, 15);
		panel.add(lblPais, gbc_lblPais);

		cbPaises = new JComboBox<Pais>();

		for (int i = 0; i < paises.size(); i++)
			if (provinciaController.cantidadProvincias(paises.get(i)) != 0)
				cbPaises.addItem(paises.get(i));
		cbPaises.setSelectedIndex(posPaisSeleccionado);
		cbPaises.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				provincias = provinciaController.obtenerProvincias((Pais) cbPaises.getSelectedItem());
				actualizar();
			}
		});

		GridBagConstraints gbc_cbPaises = new GridBagConstraints();
		gbc_cbPaises.gridx = 1;
		gbc_cbPaises.gridy = 0;
		gbc_cbPaises.gridwidth = 1;
		gbc_cbPaises.gridheight = 1;
		gbc_cbPaises.weightx = 0.5;
		gbc_cbPaises.weighty = 0.0;
		gbc_cbPaises.fill = GridBagConstraints.BOTH;
		gbc_cbPaises.insets = new Insets(15, 0, 10, 15);
		panel.add(cbPaises, gbc_cbPaises);

		JLabel lblProvincia = new JLabel("Provincia");
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.gridx = 0;
		gbc_lblProvincia.gridy = 1;
		gbc_lblProvincia.gridwidth = 1;
		gbc_lblProvincia.gridheight = 1;
		gbc_lblProvincia.weightx = 0.5;
		gbc_lblProvincia.weighty = 0.0;
		gbc_lblProvincia.fill = GridBagConstraints.BOTH;
		gbc_lblProvincia.insets = new Insets(0, 15, 10, 15);
		panel.add(lblProvincia, gbc_lblProvincia);

		cbProvincias = new JComboBox<Provincia>();
		actualizar();
		cbProvincias.setSelectedIndex(posProvinciaSeleccionada);
		GridBagConstraints gbc_cbProvincias = new GridBagConstraints();
		gbc_cbProvincias.gridx = 1;
		gbc_cbProvincias.gridy = 1;
		gbc_cbProvincias.gridwidth = 1;
		gbc_cbProvincias.gridheight = 1;
		gbc_cbProvincias.weightx = 0.5;
		gbc_cbProvincias.weighty = 0.0;
		gbc_cbProvincias.fill = GridBagConstraints.BOTH;
		gbc_cbProvincias.insets = new Insets(0, 0, 10, 15);
		panel.add(cbProvincias, gbc_cbProvincias);

		JLabel lblLocalidad = new JLabel("Localidad");
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.gridx = 0;
		gbc_lblLocalidad.gridy = 2;
		gbc_lblLocalidad.gridwidth = 1;
		gbc_lblLocalidad.gridheight = 1;
		gbc_lblLocalidad.weightx = 0.5;
		gbc_lblLocalidad.weighty = 0.0;
		gbc_lblLocalidad.fill = GridBagConstraints.BOTH;
		gbc_lblLocalidad.insets = new Insets(15, 15, 10, 15);
		panel.add(lblLocalidad, gbc_lblLocalidad);

		tfLocalizacion = new JTextField();
		GridBagConstraints gbc_tfLocalizacion = new GridBagConstraints();
		gbc_tfLocalizacion.gridx = 1;
		gbc_tfLocalizacion.gridy = 2;
		gbc_tfLocalizacion.gridwidth = 1;
		gbc_tfLocalizacion.gridheight = 1;
		gbc_tfLocalizacion.weightx = 0.5;
		gbc_tfLocalizacion.weighty = 0.0;
		gbc_tfLocalizacion.fill = GridBagConstraints.BOTH;
		gbc_tfLocalizacion.insets = new Insets(15, 0, 10, 15);
		panel.add(tfLocalizacion, gbc_tfLocalizacion);

		JLabel lblCodPostal = new JLabel("Cod. Postal");
		GridBagConstraints gbc_lblCodPostal = new GridBagConstraints();
		gbc_lblCodPostal.gridx = 0;
		gbc_lblCodPostal.gridy = 3;
		gbc_lblCodPostal.gridwidth = 1;
		gbc_lblCodPostal.gridheight = 1;
		gbc_lblCodPostal.weightx = 0.5;
		gbc_lblCodPostal.weighty = 0.0;
		gbc_lblCodPostal.fill = GridBagConstraints.BOTH;
		gbc_lblCodPostal.insets = new Insets(0, 15, 10, 15);
		panel.add(lblCodPostal, gbc_lblCodPostal);

		tfCodPostal = new JTextField();
		GridBagConstraints gbc_tfCodPostal = new GridBagConstraints();
		gbc_tfCodPostal.gridx = 1;
		gbc_tfCodPostal.gridy = 3;
		gbc_tfCodPostal.gridwidth = 1;
		gbc_tfCodPostal.gridheight = 1;
		gbc_tfCodPostal.weightx = 0.5;
		gbc_tfCodPostal.weighty = 0.0;
		gbc_tfCodPostal.fill = GridBagConstraints.BOTH;
		gbc_tfCodPostal.insets = new Insets(0, 0, 10, 15);
		panel.add(tfCodPostal, gbc_tfCodPostal);

		JLabel lblPrefTelefnico = new JLabel("Pref. Telef\u00F3nico");
		GridBagConstraints gbc_lblPrefTelefnico = new GridBagConstraints();
		gbc_lblPrefTelefnico.gridx = 0;
		gbc_lblPrefTelefnico.gridy = 4;
		gbc_lblPrefTelefnico.gridwidth = 1;
		gbc_lblPrefTelefnico.gridheight = 1;
		gbc_lblPrefTelefnico.weightx = 0.5;
		gbc_lblPrefTelefnico.weighty = 0.0;
		gbc_lblPrefTelefnico.fill = GridBagConstraints.BOTH;
		gbc_lblPrefTelefnico.insets = new Insets(0, 15, 10, 15);
		panel.add(lblPrefTelefnico, gbc_lblPrefTelefnico);

		tfPrefTelefonico = new JTextField();
		GridBagConstraints gbc_tfPrefTelefonico = new GridBagConstraints();
		gbc_tfPrefTelefonico.gridx = 1;
		gbc_tfPrefTelefonico.gridy = 4;
		gbc_tfPrefTelefonico.gridwidth = 1;
		gbc_tfPrefTelefonico.gridheight = 1;
		gbc_tfPrefTelefonico.weightx = 0.5;
		gbc_tfPrefTelefonico.weighty = 0.0;
		gbc_tfPrefTelefonico.fill = GridBagConstraints.BOTH;
		gbc_tfPrefTelefonico.insets = new Insets(0, 0, 10, 15);
		panel.add(tfPrefTelefonico, gbc_tfPrefTelefonico);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 5;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.5;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 5;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 0.5;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 6;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.5;
		gbc_btnCancelar.weighty = 1.0;
		gbc_btnCancelar.anchor = GridBagConstraints.CENTER;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(15, 60, 15, 15);
		panel.add(btnCancelar, gbc_btnCancelar);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 6;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 0.5;
		gbc_btnGrabar.weighty = 1.0;
		gbc_btnGrabar.anchor = GridBagConstraints.CENTER;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(15, 15, 15, 15);
		panel.add(btnGrabar, gbc_btnGrabar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}

	private void actualizar() {
		cbProvincias.removeAllItems();
		for (int i = 0; i < provincias.size(); i++)
			cbProvincias.addItem(provincias.get(i));
	}
}