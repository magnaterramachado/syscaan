package view.dialog.administracion;

import java.awt.GridBagLayout;
import java.awt.SystemColor;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import com.toedter.calendar.DateUtil;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import admin.SistemaCAAN;
import controller.PerroController;
import controller.PerroEstadoController;
import controller.PerroRazaController;
import controller.PersonalController;
import controller.SectorController;
import model.Perro;
import model.PerroEstado;
import model.PerroRaza;
import model.Personal;
import model.Sector;
import javax.swing.JButton;
import view.panel.administracion.PanelPerros;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;

public class AgregarPerroDialog extends JDialog {

	private static final long serialVersionUID = 8447831717256433299L;
	private PanelPerros parentPanel;
	private JDialog dialogo;
	private JPanel panel;
	private JTextField tfNombre, tfEdadAprox, tfColor, tfObservaciones, tfLegajo;
	private JDateChooser dcFechaIngreso, dcFechaNacimiento, dcFechaBaja;
	private JComboBox<Personal> cbResponsableAsignado;
	private JComboBox<String> cbGenero;
	private JComboBox<Sector> cbSector;
	private JComboBox<PerroEstado> cbEstado;
	private JComboBox<PerroRaza> cbRaza;
	private JButton btnGrabar, btnCancelar;
	private SectorController sectorController;
	private List<Sector> listaSector;
	private PerroEstadoController perroEstadoController;
	private List<PerroEstado> listaEstado;
	private PerroRazaController perroRazaController;
	private List<PerroRaza> listaRaza;
	private PersonalController personalController;
	private List<Personal> listaPersonal;

	public AgregarPerroDialog(JFrame f_ppal, PanelPerros parentPanel) {
		super(f_ppal, ModalityType.APPLICATION_MODAL);
		this.dialogo = this;
		this.parentPanel = parentPanel;
		setTitle("Grabar perro");
		setBackground(SystemColor.window);
		perroEstadoController = PerroEstadoController.getInstance();
		listaEstado = perroEstadoController.obtenerEstados();
		perroRazaController = PerroRazaController.getInstance();
		listaRaza = perroRazaController.obtenerRazas();
		personalController = PersonalController.getInstance();
		listaPersonal = personalController.obtenerListaPersonal();
		sectorController = SectorController.getInstance();
		listaSector = sectorController.obtenerSectores();

		this.panel = new JPanel(new GridBagLayout());
		add(panel);
		cargarComponentes();
		actionListeners();

		setLocationRelativeTo(f_ppal);
		setLocation(f_ppal.getWidth() / 2, f_ppal.getHeight() / 8);
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void actionListeners() {
		cbEstado.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (!((PerroEstado) cbEstado.getSelectedItem()).isPerroActivo())
					dcFechaBaja.setEnabled(true);
				else {
					dcFechaBaja.setEnabled(false);
					dcFechaBaja.setDate(null);
				}
			}
		});

		((JTextFieldDateEditor) dcFechaNacimiento.getDateEditor()).addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				tfEdadAprox.setText(null);
			}
		});

		tfEdadAprox.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				dcFechaNacimiento.setDate(null);
			}
		});

		dcFechaNacimiento.getCalendarButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tfEdadAprox.setText(null);
			}
		});

		tfEdadAprox.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				dcFechaNacimiento.setDate(null);
			}
		});

		btnGrabar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (grabarPerro())
					dispose();
			}

			private boolean grabarPerro() {
				DateUtil dateUtil = new DateUtil();
				PerroController perroController = PerroController.getInstance();

				if (tfLegajo.getText() == null || tfLegajo.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Legajo\" no puede quedar vac\u00edo.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (SistemaCAAN.convertInt(tfLegajo.getText()) == null
						|| SistemaCAAN.convertInt(tfLegajo.getText()) <= 0) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Legajo\" debe ser un n\u00famero mayor a 0.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (perroController.obtenerPerro(SistemaCAAN.convertInt(tfLegajo.getText())) != null) {
					JOptionPane.showMessageDialog(dialogo,
							"El N\u00famero de Legajo " + SistemaCAAN.convertInt(tfLegajo.getText()) + " ya existe.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (tfNombre.getText() == null || tfNombre.getText().equals("")) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Nombre\" no puede quedar vac\u00edo.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (((JTextFieldDateEditor) dcFechaIngreso.getDateEditor()).getText().equals("__/__/____")) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Fecha de Ingreso\" no puede quedar vac\u00edo.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (dcFechaIngreso.getDate() == null || !dateUtil.checkDate(dcFechaIngreso.getDate())) {
					JOptionPane.showMessageDialog(dialogo, "El campo \"Fecha de Ingreso\" contiene una fecha invalida.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (dcFechaIngreso.getDate().after(new Date())) {
					JOptionPane.showMessageDialog(dialogo,
							"La \"Fecha de Ingreso\" no puede ser posterior a la fecha actual.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if ((((JTextFieldDateEditor) dcFechaNacimiento.getDateEditor()).getText().equals("__/__/____"))
						&& (tfEdadAprox.getText() == null || tfEdadAprox.getText().equals(""))) {
					JOptionPane.showMessageDialog(dialogo,
							"Los campos \"Fecha de Nacimiento\" y \"Edad aproximada\" no pueden quedar ambos vac\u00edo. Debe completar al menos uno de ellos.",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;
				} else if ((tfEdadAprox.getText() == null || tfEdadAprox.getText().equals(""))
						&& (dcFechaNacimiento.getDate() == null || !dateUtil.checkDate(dcFechaNacimiento.getDate()))) {
					JOptionPane.showMessageDialog(dialogo,
							"El campo \"Fecha de Nacimiento\" contiene una fecha invalida.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				} else if ((((JTextFieldDateEditor) dcFechaNacimiento.getDateEditor()).getText().equals("__/__/____"))
						&& (SistemaCAAN.convertInt(tfEdadAprox.getText()) == null
								|| SistemaCAAN.convertInt(tfEdadAprox.getText()) <= 0)) {
					JOptionPane.showMessageDialog(dialogo,
							"El campo \"Edad aproximada\" debe ser un n\u00famero mayor que 0.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (dcFechaNacimiento.getDate() != null && dcFechaNacimiento.getDate().after(new Date())) {
					JOptionPane.showMessageDialog(dialogo,
							"La \"Fecha de Nacimiento\" no puede ser posterior a la fecha actual.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				} else if (dcFechaNacimiento.getDate() != null
						&& dcFechaNacimiento.getDate().after(dcFechaIngreso.getDate())) {
					JOptionPane.showMessageDialog(dialogo,
							"La \"Fecha de Nacimiento\" no puede ser posterior a la fecha de ingreso.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (!((PerroEstado) cbEstado.getSelectedItem()).isPerroActivo()) {
					if (dcFechaBaja.getDate() == null || !dateUtil.checkDate(dcFechaBaja.getDate())) {
						JOptionPane.showMessageDialog(dialogo,
								"Si el Estado es Inactivo la \"Fecha de Baja\" no puede quedar incompleta.", "Error",
								JOptionPane.ERROR_MESSAGE);
						return false;
					} else if (dcFechaBaja.getDate() != null && dcFechaBaja.getDate().after(new Date())) {
						JOptionPane.showMessageDialog(dialogo,
								"La \"Fecha de Baja\" no puede ser posterior a la fecha actual.", "Error",
								JOptionPane.ERROR_MESSAGE);
						return false;
					} else if (dcFechaBaja.getDate() != null
							&& dcFechaBaja.getDate().before(dcFechaNacimiento.getDate())) {
						JOptionPane.showMessageDialog(dialogo,
								"La \"Fecha de Baja\" no puede ser anterior a la fecha de nacimiento.", "Error",
								JOptionPane.ERROR_MESSAGE);
						return false;
					} else if (dcFechaBaja.getDate() != null
							&& dcFechaBaja.getDate().before(dcFechaIngreso.getDate())) {
						JOptionPane.showMessageDialog(dialogo,
								"La \"Fecha de Baja\" no puede ser anterior a la fecha de ingreso.", "Error",
								JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}

				Calendar fecNac = Calendar.getInstance();

				if (tfEdadAprox.getText() != null && !tfEdadAprox.getText().equals("")) {
					fecNac.setTime(new Date());
					fecNac.add(Calendar.YEAR, -SistemaCAAN.convertInt(tfEdadAprox.getText()));
				} else
					fecNac.setTime(dcFechaNacimiento.getDate());
				fecNac.set(Calendar.MILLISECOND, 0);
				fecNac.set(Calendar.SECOND, 0);
				fecNac.set(Calendar.MINUTE, 0);
				fecNac.set(Calendar.HOUR, 0);

				Calendar fecIng = Calendar.getInstance();
				fecIng.setTime(dcFechaIngreso.getDate());
				fecIng.set(Calendar.MILLISECOND, 0);
				fecIng.set(Calendar.SECOND, 0);
				fecIng.set(Calendar.MINUTE, 0);
				fecIng.set(Calendar.HOUR, 0);

				Integer legajo = SistemaCAAN.convertInt(tfLegajo.getText());

				Perro perro = new Perro(legajo, fecIng.getTime(), tfNombre.getText(),
						cbGenero.getSelectedItem().toString().charAt(0), (PerroRaza) cbRaza.getSelectedItem(),
						(PerroEstado) cbEstado.getSelectedItem(), (Sector) cbSector.getSelectedItem(),
						fecNac.getTime());

				if (cbResponsableAsignado.getSelectedIndex() != 0)
					perro.setResponsable((Personal) cbResponsableAsignado.getSelectedItem());

				if (tfColor.getText() != null && !tfColor.getText().equals(""))
					perro.setColor(tfColor.getText());

				if (dcFechaBaja.getDate() != null && dateUtil.checkDate(dcFechaBaja.getDate())) {
					Calendar fecDef = Calendar.getInstance();
					fecDef.setTime(dcFechaBaja.getDate());
					perro.setFechaBaja(fecDef.getTime());
				}

				if (tfObservaciones.getText() != null && !tfObservaciones.getText().equals(""))
					perro.setObservacion(tfObservaciones.getText());

				if (perroController.agregar(perro)) {
					JOptionPane.showMessageDialog(dialogo,
							"El perro \"" + tfNombre.getText() + "\" fue grabado con \u00e9xito.",
							"Operaci\u00f3n concreatada", JOptionPane.INFORMATION_MESSAGE);
					parentPanel.actualizar();
					return true;
				} else {
					JOptionPane.showMessageDialog(dialogo,
							"Hubo un error al procesar la transacci\u00f3n.\nRevise si \"" + tfNombre.getText()
									+ "\" no se encontraba previamente ingresado.",
							"Error", JOptionPane.WARNING_MESSAGE);
					return false;
				}
			}

		});

		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void cargarComponentes() {
		JLabel lblLegajo = new JLabel("Legajo");
		GridBagConstraints gbc_lblLegajo = new GridBagConstraints();
		gbc_lblLegajo.gridx = 0;
		gbc_lblLegajo.gridy = 0;
		gbc_lblLegajo.gridwidth = 1;
		gbc_lblLegajo.gridheight = 1;
		gbc_lblLegajo.weightx = 0.0;
		gbc_lblLegajo.weighty = 0.0;
		gbc_lblLegajo.fill = GridBagConstraints.BOTH;
		gbc_lblLegajo.anchor = GridBagConstraints.WEST;
		gbc_lblLegajo.insets = new Insets(15, 15, 10, 15);
		panel.add(lblLegajo, gbc_lblLegajo);

		tfLegajo = new JTextField();
		GridBagConstraints gbc_tfLegajo = new GridBagConstraints();
		gbc_tfLegajo.gridx = 1;
		gbc_tfLegajo.gridy = 0;
		gbc_tfLegajo.gridwidth = 1;
		gbc_tfLegajo.gridheight = 1;
		gbc_tfLegajo.weightx = 1.0;
		gbc_tfLegajo.weighty = 0.0;
		gbc_tfLegajo.fill = GridBagConstraints.BOTH;
		gbc_tfLegajo.insets = new Insets(15, 0, 10, 15);
		panel.add(tfLegajo, gbc_tfLegajo);

		JLabel lblNombre = new JLabel("Nombre");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.gridx = 0;
		gbc_lblNombre.gridy = 1;
		gbc_lblNombre.gridwidth = 1;
		gbc_lblNombre.gridheight = 1;
		gbc_lblNombre.weightx = 0.0;
		gbc_lblNombre.weighty = 0.0;
		gbc_lblNombre.fill = GridBagConstraints.BOTH;
		gbc_lblNombre.anchor = GridBagConstraints.WEST;
		gbc_lblNombre.insets = new Insets(0, 15, 10, 15);
		panel.add(lblNombre, gbc_lblNombre);

		tfNombre = new JTextField();
		GridBagConstraints gbc_tfNombre = new GridBagConstraints();
		gbc_tfNombre.gridx = 1;
		gbc_tfNombre.gridy = 1;
		gbc_tfNombre.gridwidth = 1;
		gbc_tfNombre.gridheight = 1;
		gbc_tfNombre.weightx = 1.0;
		gbc_tfNombre.weighty = 0.0;
		gbc_tfNombre.fill = GridBagConstraints.BOTH;
		gbc_tfNombre.insets = new Insets(0, 0, 10, 15);
		panel.add(tfNombre, gbc_tfNombre);

		JLabel lblFechaIngreso = new JLabel("Fecha de ingreso");
		GridBagConstraints gbc_lblFechaIngreso = new GridBagConstraints();
		gbc_lblFechaIngreso.gridx = 0;
		gbc_lblFechaIngreso.gridy = 2;
		gbc_lblFechaIngreso.gridwidth = 1;
		gbc_lblFechaIngreso.gridheight = 1;
		gbc_lblFechaIngreso.weightx = 0.0;
		gbc_lblFechaIngreso.weighty = 0.0;
		gbc_lblFechaIngreso.fill = GridBagConstraints.BOTH;
		gbc_lblFechaIngreso.anchor = GridBagConstraints.WEST;
		gbc_lblFechaIngreso.insets = new Insets(0, 15, 10, 15);
		panel.add(lblFechaIngreso, gbc_lblFechaIngreso);

		dcFechaIngreso = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
		dcFechaIngreso.setIcon(new ImageIcon("icon/calendar15x15.png"));
		GridBagConstraints gbc_dcFechaIngreso = new GridBagConstraints();
		gbc_dcFechaIngreso.gridx = 1;
		gbc_dcFechaIngreso.gridy = 2;
		gbc_dcFechaIngreso.gridwidth = 1;
		gbc_dcFechaIngreso.gridheight = 1;
		gbc_dcFechaIngreso.weightx = 1.0;
		gbc_dcFechaIngreso.weighty = 0.0;
		gbc_dcFechaIngreso.fill = GridBagConstraints.BOTH;
		gbc_dcFechaIngreso.insets = new Insets(0, 0, 10, 15);
		panel.add(dcFechaIngreso, gbc_dcFechaIngreso);

		JLabel lblResponsableAsignado = new JLabel("Responsable asignado");
		GridBagConstraints gbc_lblResponsableAsignado = new GridBagConstraints();
		gbc_lblResponsableAsignado.fill = GridBagConstraints.VERTICAL;
		gbc_lblResponsableAsignado.gridx = 0;
		gbc_lblResponsableAsignado.gridy = 3;
		gbc_lblResponsableAsignado.weightx = 0.0;
		gbc_lblResponsableAsignado.weighty = 0.0;
		gbc_lblResponsableAsignado.anchor = GridBagConstraints.WEST;
		gbc_lblResponsableAsignado.insets = new Insets(0, 15, 10, 15);
		panel.add(lblResponsableAsignado, gbc_lblResponsableAsignado);

		cbResponsableAsignado = new JComboBox<Personal>();
		cbResponsableAsignado.addItem(new Personal(0, "Ninguno", ""));
		for (int i = 0; i < listaPersonal.size(); i++)
			cbResponsableAsignado.addItem(listaPersonal.get(i));
		GridBagConstraints gbc_cbResponsableAsignado = new GridBagConstraints();
		gbc_cbResponsableAsignado.insets = new Insets(0, 0, 10, 15);
		gbc_cbResponsableAsignado.fill = GridBagConstraints.BOTH;
		gbc_cbResponsableAsignado.weightx = 1.0;
		gbc_cbResponsableAsignado.weighty = 0.0;
		gbc_cbResponsableAsignado.gridx = 1;
		gbc_cbResponsableAsignado.gridy = 3;
		panel.add(cbResponsableAsignado, gbc_cbResponsableAsignado);

		JLabel lblGenero = new JLabel("Genero");
		GridBagConstraints gbc_lblGenero = new GridBagConstraints();
		gbc_lblGenero.fill = GridBagConstraints.VERTICAL;
		gbc_lblGenero.gridx = 0;
		gbc_lblGenero.gridy = 4;
		gbc_lblGenero.weightx = 0.0;
		gbc_lblGenero.weighty = 0.0;
		gbc_lblGenero.anchor = GridBagConstraints.WEST;
		gbc_lblGenero.insets = new Insets(0, 15, 10, 15);
		panel.add(lblGenero, gbc_lblGenero);

		cbGenero = new JComboBox<String>();
		cbGenero.setModel(new DefaultComboBoxModel<String>(new String[] { "Hembra", "Macho" }));
		GridBagConstraints gbc_cbGenero = new GridBagConstraints();
		gbc_cbGenero.gridx = 1;
		gbc_cbGenero.gridy = 4;
		gbc_cbGenero.weightx = 1.0;
		gbc_cbGenero.weighty = 0.0;
		gbc_cbGenero.fill = GridBagConstraints.BOTH;
		gbc_cbGenero.insets = new Insets(0, 0, 10, 15);
		panel.add(cbGenero, gbc_cbGenero);

		JLabel lblSector = new JLabel("Sector");
		GridBagConstraints gbc_lblSector = new GridBagConstraints();
		gbc_lblSector.fill = GridBagConstraints.VERTICAL;
		gbc_lblSector.gridx = 0;
		gbc_lblSector.gridy = 5;
		gbc_lblSector.weightx = 0.0;
		gbc_lblSector.weighty = 0.0;
		gbc_lblSector.anchor = GridBagConstraints.WEST;
		gbc_lblSector.insets = new Insets(0, 15, 10, 15);
		panel.add(lblSector, gbc_lblSector);

		cbSector = new JComboBox<Sector>();
		for (int i = 0; i < listaSector.size(); i++)
			cbSector.addItem(listaSector.get(i));
		GridBagConstraints gbc_cbSector = new GridBagConstraints();
		gbc_cbSector.gridx = 1;
		gbc_cbSector.gridy = 5;
		gbc_cbSector.weightx = 1.0;
		gbc_cbSector.weighty = 0.0;
		gbc_cbSector.fill = GridBagConstraints.BOTH;
		gbc_cbSector.insets = new Insets(0, 0, 10, 15);
		panel.add(cbSector, gbc_cbSector);

		JLabel lblEstado = new JLabel("Estado");
		GridBagConstraints gbc_lblEstado = new GridBagConstraints();
		gbc_lblEstado.fill = GridBagConstraints.VERTICAL;
		gbc_lblEstado.gridx = 0;
		gbc_lblEstado.gridy = 6;
		gbc_lblEstado.weightx = 0.0;
		gbc_lblEstado.weighty = 0.0;
		gbc_lblEstado.anchor = GridBagConstraints.WEST;
		gbc_lblEstado.insets = new Insets(0, 15, 10, 15);
		panel.add(lblEstado, gbc_lblEstado);

		cbEstado = new JComboBox<PerroEstado>();
		for (int i = 0; i < listaEstado.size(); i++) {
			listaEstado.get(i).setEtiqueta(false);
			cbEstado.addItem(listaEstado.get(i));
		}
		GridBagConstraints gbc_cbEstado = new GridBagConstraints();
		gbc_cbEstado.gridx = 1;
		gbc_cbEstado.gridy = 6;
		gbc_cbEstado.weightx = 1.0;
		gbc_cbEstado.weighty = 0.0;
		gbc_cbEstado.fill = GridBagConstraints.BOTH;
		gbc_cbEstado.insets = new Insets(0, 0, 10, 15);
		panel.add(cbEstado, gbc_cbEstado);

		JLabel lblRaza = new JLabel("Raza");
		GridBagConstraints gbc_lblRaza = new GridBagConstraints();
		gbc_lblRaza.fill = GridBagConstraints.VERTICAL;
		gbc_lblRaza.gridx = 0;
		gbc_lblRaza.gridy = 7;
		gbc_lblRaza.weightx = 0.0;
		gbc_lblRaza.weighty = 0.0;
		gbc_lblRaza.anchor = GridBagConstraints.WEST;
		gbc_lblRaza.insets = new Insets(0, 15, 10, 15);
		panel.add(lblRaza, gbc_lblRaza);

		cbRaza = new JComboBox<PerroRaza>();
		for (int i = 0; i < listaRaza.size(); i++)
			cbRaza.addItem(listaRaza.get(i));
		GridBagConstraints gbc_cbRaza = new GridBagConstraints();
		gbc_cbRaza.gridx = 1;
		gbc_cbRaza.gridy = 7;
		gbc_cbRaza.weightx = 1.0;
		gbc_cbRaza.weighty = 0.0;
		gbc_cbRaza.fill = GridBagConstraints.BOTH;
		gbc_cbRaza.insets = new Insets(0, 0, 10, 15);
		panel.add(cbRaza, gbc_cbRaza);

		JLabel lblFechaNacimiento = new JLabel("Fecha de nacimiento");
		GridBagConstraints gbc_lblFechaNacimiento = new GridBagConstraints();
		gbc_lblFechaNacimiento.gridx = 0;
		gbc_lblFechaNacimiento.gridy = 8;
		gbc_lblFechaNacimiento.weightx = 0.0;
		gbc_lblFechaNacimiento.weighty = 0.0;
		gbc_lblFechaNacimiento.anchor = GridBagConstraints.WEST;
		gbc_lblFechaNacimiento.fill = GridBagConstraints.BOTH;
		gbc_lblFechaNacimiento.insets = new Insets(0, 15, 10, 15);
		panel.add(lblFechaNacimiento, gbc_lblFechaNacimiento);

		dcFechaNacimiento = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
		dcFechaNacimiento.setIcon(new ImageIcon("icon/calendar15x15.png"));

		GridBagConstraints gbc_dcFechaNacimiento = new GridBagConstraints();
		gbc_dcFechaNacimiento.gridx = 1;
		gbc_dcFechaNacimiento.gridy = 8;
		gbc_dcFechaNacimiento.gridwidth = 1;
		gbc_dcFechaNacimiento.gridheight = 1;
		gbc_dcFechaNacimiento.weightx = 1.0;
		gbc_dcFechaNacimiento.weighty = 0.0;
		gbc_dcFechaNacimiento.fill = GridBagConstraints.BOTH;
		gbc_dcFechaNacimiento.insets = new Insets(0, 0, 10, 15);
		panel.add(dcFechaNacimiento, gbc_dcFechaNacimiento);

		JLabel lblFechaBaja = new JLabel("Fecha de Baja");
		GridBagConstraints gbc_lblFechaBaja = new GridBagConstraints();
		gbc_lblFechaBaja.gridx = 0;
		gbc_lblFechaBaja.gridy = 9;
		gbc_lblFechaBaja.weightx = 0.0;
		gbc_lblFechaBaja.weighty = 0.0;
		gbc_lblFechaBaja.anchor = GridBagConstraints.WEST;
		gbc_lblFechaBaja.insets = new Insets(0, 15, 10, 15);
		panel.add(lblFechaBaja, gbc_lblFechaBaja);

		dcFechaBaja = new JDateChooser("dd/MM/yyyy", "##/##/####", '_');
		dcFechaBaja.setEnabled(false);
		dcFechaBaja.setIcon(new ImageIcon("icon/calendar15x15.png"));
		GridBagConstraints gbc_dcFechaBaja = new GridBagConstraints();
		gbc_dcFechaBaja.gridx = 1;
		gbc_dcFechaBaja.gridy = 9;
		gbc_dcFechaBaja.weightx = 1.0;
		gbc_dcFechaBaja.weighty = 0.0;
		gbc_dcFechaBaja.fill = GridBagConstraints.BOTH;
		gbc_dcFechaBaja.insets = new Insets(0, 0, 10, 15);
		panel.add(dcFechaBaja, gbc_dcFechaBaja);

		JLabel lblEdadAprox = new JLabel("Edad aproximada");
		GridBagConstraints gbc_lblEdadAprox = new GridBagConstraints();
		gbc_lblEdadAprox.gridx = 0;
		gbc_lblEdadAprox.gridy = 10;
		gbc_lblEdadAprox.weightx = 0.0;
		gbc_lblEdadAprox.weighty = 0.0;
		gbc_lblEdadAprox.anchor = GridBagConstraints.WEST;
		gbc_lblEdadAprox.fill = GridBagConstraints.BOTH;
		gbc_lblEdadAprox.insets = new Insets(0, 15, 10, 15);
		panel.add(lblEdadAprox, gbc_lblEdadAprox);

		tfEdadAprox = new JTextField();
		GridBagConstraints gbc_txtTfedadaprox = new GridBagConstraints();
		gbc_txtTfedadaprox.gridx = 1;
		gbc_txtTfedadaprox.gridy = 10;
		gbc_txtTfedadaprox.weightx = 1.0;
		gbc_txtTfedadaprox.weighty = 0.0;
		gbc_txtTfedadaprox.fill = GridBagConstraints.BOTH;
		gbc_txtTfedadaprox.insets = new Insets(0, 0, 10, 15);
		panel.add(tfEdadAprox, gbc_txtTfedadaprox);

		JLabel lblColor = new JLabel("Color");
		GridBagConstraints gbc_lblColor = new GridBagConstraints();
		gbc_lblColor.gridx = 0;
		gbc_lblColor.gridy = 11;
		gbc_lblColor.weightx = 0.0;
		gbc_lblColor.weighty = 0.0;
		gbc_lblColor.anchor = GridBagConstraints.WEST;
		gbc_lblColor.fill = GridBagConstraints.BOTH;
		gbc_lblColor.insets = new Insets(0, 15, 10, 15);
		panel.add(lblColor, gbc_lblColor);

		tfColor = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 11;
		gbc_textField.weightx = 1.0;
		gbc_textField.weighty = 0.0;
		gbc_textField.fill = GridBagConstraints.BOTH;
		gbc_textField.insets = new Insets(0, 0, 10, 15);
		panel.add(tfColor, gbc_textField);

		JLabel lblObservaciones = new JLabel("Observaciones");
		GridBagConstraints gbc_lblObservaciones = new GridBagConstraints();
		gbc_lblObservaciones.gridx = 0;
		gbc_lblObservaciones.gridy = 12;
		gbc_lblObservaciones.gridwidth = 1;
		gbc_lblObservaciones.gridheight = 1;
		gbc_lblObservaciones.weightx = 0.0;
		gbc_lblObservaciones.weighty = 0.0;
		gbc_lblObservaciones.fill = GridBagConstraints.BOTH;
		gbc_lblObservaciones.anchor = GridBagConstraints.WEST;
		gbc_lblObservaciones.insets = new Insets(0, 15, 10, 15);
		panel.add(lblObservaciones, gbc_lblObservaciones);

		tfObservaciones = new JTextField();
		GridBagConstraints gbc_tfObservaciones = new GridBagConstraints();
		gbc_tfObservaciones.gridx = 1;
		gbc_tfObservaciones.gridy = 12;
		gbc_tfObservaciones.gridwidth = 1;
		gbc_tfObservaciones.gridheight = 1;
		gbc_tfObservaciones.weightx = 1.0;
		gbc_tfObservaciones.weighty = 0.0;
		gbc_tfObservaciones.fill = GridBagConstraints.BOTH;
		gbc_tfObservaciones.insets = new Insets(0, 0, 10, 15);
		panel.add(tfObservaciones, gbc_tfObservaciones);

		JPanel panelBotones = new JPanel();
		panelBotones.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelBotones = new GridBagConstraints();
		gbc_panelBotones.gridx = 1;
		gbc_panelBotones.gridy = 13;
		gbc_panelBotones.gridwidth = 2;
		gbc_panelBotones.gridheight = 1;
		gbc_panelBotones.weightx = 0.0;
		gbc_panelBotones.weighty = 0.0;
		gbc_panelBotones.anchor = GridBagConstraints.EAST;
		gbc_panelBotones.fill = GridBagConstraints.BOTH;
		gbc_panelBotones.insets = new Insets(0, 0, 0, 15);
		panel.add(panelBotones, gbc_panelBotones);

		btnGrabar = new JButton("Grabar");
		GridBagConstraints gbc_btnGrabar = new GridBagConstraints();
		gbc_btnGrabar.gridx = 0;
		gbc_btnGrabar.gridy = 0;
		gbc_btnGrabar.gridwidth = 1;
		gbc_btnGrabar.gridheight = 1;
		gbc_btnGrabar.weightx = 1.0;
		gbc_btnGrabar.weighty = 0.0;
		gbc_btnGrabar.anchor = GridBagConstraints.EAST;
		gbc_btnGrabar.fill = GridBagConstraints.NONE;
		gbc_btnGrabar.insets = new Insets(0, 0, 15, 15);
		panelBotones.add(btnGrabar, gbc_btnGrabar);

		btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 0;
		gbc_btnCancelar.gridwidth = 1;
		gbc_btnCancelar.gridheight = 1;
		gbc_btnCancelar.weightx = 0.0;
		gbc_btnCancelar.weighty = 0.0;
		gbc_btnCancelar.anchor = GridBagConstraints.EAST;
		gbc_btnCancelar.fill = GridBagConstraints.NONE;
		gbc_btnCancelar.insets = new Insets(0, 0, 15, 0);
		panelBotones.add(btnCancelar, gbc_btnCancelar);

		btnGrabar.setPreferredSize(btnCancelar.getPreferredSize());
		btnCancelar.setPreferredSize(btnCancelar.getPreferredSize());
	}
}