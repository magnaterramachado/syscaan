package view.panel.configuracion;

import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import controller.PerroEstadoController;
import model.PerroEstado;
import view.VentanaPrincipal;
import view.dialog.configuracion.AgregarPerroEstadoDialog;
import view.dialog.configuracion.EditarPerroEstadoDialog;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.GridBagConstraints;

public class PanelEstadosPerro extends JPanel {

	private static final long serialVersionUID = -9162110791621444898L;
	private PanelEstadosPerro pr;
	private JList<PerroEstado> listEstados;
	private DefaultListModel<PerroEstado> dlm;
	private List<PerroEstado> listaEstados;
	private JButton agregar, remover, editar;
	private PerroEstadoController perroEstadoController;

	public PanelEstadosPerro() {
		perroEstadoController = PerroEstadoController.getInstance();
		setLayout(new GridBagLayout());
		cargarComponentes();
		actualizar();
		actionListeners();
		pr = this;
	}

	private void cargarComponentes() {
		listEstados = new JList<PerroEstado>();
		JScrollPane scroll = new JScrollPane(listEstados);
		scroll.setPreferredSize(new Dimension(150, scroll.getHeight()));
		GridBagConstraints gbc_listEstados = new GridBagConstraints();
		gbc_listEstados.gridx = 0;
		gbc_listEstados.gridy = 0;
		gbc_listEstados.gridwidth = 1;
		gbc_listEstados.gridheight = 4;
		gbc_listEstados.weightx = 0.4;
		gbc_listEstados.weighty = 1.0;
		gbc_listEstados.anchor = GridBagConstraints.NORTHWEST;
		gbc_listEstados.fill = GridBagConstraints.BOTH;
		gbc_listEstados.insets = new Insets(15, 15, 200, 40);
		add(scroll, gbc_listEstados);

		agregar = new JButton("Agregar estado", new ImageIcon("icon/agregar20x20.png"));
		GridBagConstraints gbc_agregar = new GridBagConstraints();
		gbc_agregar.gridx = 1;
		gbc_agregar.gridy = 0;
		gbc_agregar.gridwidth = 1;
		gbc_agregar.gridheight = 1;
		gbc_agregar.weightx = 0.6;
		gbc_agregar.weighty = 0.0;
		gbc_agregar.anchor = GridBagConstraints.NORTHWEST;
		gbc_agregar.fill = GridBagConstraints.NONE;
		gbc_agregar.insets = new Insets(15, 0, 5, 0);
		add(agregar, gbc_agregar);

		remover = new JButton("Remover estado", new ImageIcon("icon/remover20x20.png"));
		GridBagConstraints gbc_remover = new GridBagConstraints();
		gbc_remover.gridx = 1;
		gbc_remover.gridy = 1;
		gbc_remover.gridwidth = 1;
		gbc_remover.gridheight = 1;
		gbc_remover.weightx = 0.6;
		gbc_remover.weighty = 0.0;
		gbc_remover.anchor = GridBagConstraints.NORTHWEST;
		gbc_remover.fill = GridBagConstraints.NONE;
		gbc_remover.insets = new Insets(0, 0, 5, 0);
		add(remover, gbc_remover);
		remover.setEnabled(false);

		editar = new JButton("Editar estado", new ImageIcon("icon/modificar20x20.png"));
		GridBagConstraints gbc_editar = new GridBagConstraints();
		gbc_editar.gridx = 1;
		gbc_editar.gridy = 3;
		gbc_editar.gridwidth = 1;
		gbc_editar.gridheight = 1;
		gbc_editar.weightx = 0.6;
		gbc_editar.weighty = 1.0;
		gbc_editar.anchor = GridBagConstraints.NORTHWEST;
		gbc_editar.fill = GridBagConstraints.NONE;
		gbc_editar.insets = new Insets(0, 0, 0, 0);
		add(editar, gbc_editar);
		editar.setEnabled(false);

		editar.setPreferredSize(remover.getPreferredSize());
		remover.setPreferredSize(remover.getPreferredSize());
		agregar.setPreferredSize(remover.getPreferredSize());
	}

	private void actionListeners() {
		listEstados.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (listEstados.getSelectedValue() != null && listEstados.getSelectedValue().isEliminable()) {
					remover.setEnabled(true);
					editar.setEnabled(true);
				} else {
					remover.setEnabled(false);
					editar.setEnabled(false);
				}
			}
		});
		
		agregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AgregarPerroEstadoDialog((JFrame) VentanaPrincipal.getWindows()[0],pr);
			}
		});

		remover.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (perroEstadoController.eliminar(listEstados.getSelectedValue())) {
					JOptionPane.showMessageDialog(pr,
							"El estado \"" + listEstados.getSelectedValue() + "\" fue eliminada con \u00e9xito.",
							"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
					dlm.removeElement(listEstados.getSelectedValue());
				}
			}
		});
		
		editar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EditarPerroEstadoDialog((JFrame) VentanaPrincipal.getWindows()[0],pr,listEstados.getSelectedValue());
			}
		});
	}

	public void actualizar() {
		listaEstados = perroEstadoController.obtenerEstados();
		dlm = new DefaultListModel<PerroEstado>();
		for (int i = 0; i < listaEstados.size(); i++) {
			listaEstados.get(i).setEtiqueta(true);
			dlm.addElement(listaEstados.get(i));
		}
		listEstados.setModel(dlm);
	}
}