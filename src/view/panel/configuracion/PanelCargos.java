package view.panel.configuracion;

import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import controller.PersonalCargoController;
import model.PersonalCargo;
import view.VentanaPrincipal;
import view.dialog.configuracion.AgregarPersonalCargoDialog;
import view.dialog.configuracion.EditarPersonalCargoDialog;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.GridBagConstraints;

public class PanelCargos extends JPanel {

	private static final long serialVersionUID = -9162110791621444898L;
	private PanelCargos pc;
	private JList<PersonalCargo> listCargos;
	private DefaultListModel<PersonalCargo> dlm;
	private List<PersonalCargo> listaCargos;
	private JButton agregar, remover, editar;
	private PersonalCargoController personalCargoController;

	public PanelCargos() {
		personalCargoController = PersonalCargoController.getInstance();
		setLayout(new GridBagLayout());
		cargarComponentes();
		actualizar();
		actionListeners();
		pc = this;
	}

	private void cargarComponentes() {
		listCargos = new JList<PersonalCargo>();
		JScrollPane scroll = new JScrollPane(listCargos);
		scroll.setPreferredSize(new Dimension(150, scroll.getHeight()));
		GridBagConstraints gbc_listCargos = new GridBagConstraints();
		gbc_listCargos.gridx = 0;
		gbc_listCargos.gridy = 0;
		gbc_listCargos.gridwidth = 1;
		gbc_listCargos.gridheight = 4;
		gbc_listCargos.weightx = 0.2;
		gbc_listCargos.weighty = 1.0;
		gbc_listCargos.anchor = GridBagConstraints.NORTHWEST;
		gbc_listCargos.fill = GridBagConstraints.BOTH;
		gbc_listCargos.insets = new Insets(15, 15, 200, 40);
		add(scroll, gbc_listCargos);

		agregar = new JButton("Agregar estado", new ImageIcon("icon/agregar20x20.png"));
		GridBagConstraints gbc_agregar = new GridBagConstraints();
		gbc_agregar.gridx = 1;
		gbc_agregar.gridy = 0;
		gbc_agregar.gridwidth = 1;
		gbc_agregar.gridheight = 1;
		gbc_agregar.weightx = 0.8;
		gbc_agregar.weighty = 0.0;
		gbc_agregar.anchor = GridBagConstraints.NORTHWEST;
		gbc_agregar.fill = GridBagConstraints.NONE;
		gbc_agregar.insets = new Insets(15, 0, 5, 0);
		add(agregar, gbc_agregar);

		remover = new JButton("Remover estado", new ImageIcon("icon/remover20x20.png"));
		GridBagConstraints gbc_remover = new GridBagConstraints();
		gbc_remover.gridx = 1;
		gbc_remover.gridy = 1;
		gbc_remover.gridwidth = 1;
		gbc_remover.gridheight = 1;
		gbc_remover.weightx = 0.8;
		gbc_remover.weighty = 0.0;
		gbc_remover.anchor = GridBagConstraints.NORTHWEST;
		gbc_remover.fill = GridBagConstraints.NONE;
		gbc_remover.insets = new Insets(0, 0, 5, 0);
		add(remover, gbc_remover);
		remover.setEnabled(false);

		editar = new JButton("Editar estado", new ImageIcon("icon/modificar20x20.png"));
		GridBagConstraints gbc_editar = new GridBagConstraints();
		gbc_editar.gridx = 1;
		gbc_editar.gridy = 3;
		gbc_editar.gridwidth = 1;
		gbc_editar.gridheight = 1;
		gbc_editar.weightx = 0.9;
		gbc_editar.weighty = 1.0;
		gbc_editar.anchor = GridBagConstraints.NORTHWEST;
		gbc_editar.fill = GridBagConstraints.NONE;
		gbc_editar.insets = new Insets(0, 0, 0, 0);
		add(editar, gbc_editar);
		editar.setEnabled(false);

		editar.setPreferredSize(remover.getPreferredSize());
		remover.setPreferredSize(remover.getPreferredSize());
		agregar.setPreferredSize(remover.getPreferredSize());
	}

	private void actionListeners() {
		listCargos.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (listCargos.getSelectedValue() != null) {
					remover.setEnabled(true);
					editar.setEnabled(true);
				} else {
					remover.setEnabled(false);
					editar.setEnabled(false);
				}
			}
		});

		agregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AgregarPersonalCargoDialog((JFrame) VentanaPrincipal.getWindows()[0],pc);
			}
		});

		remover.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (personalCargoController.eliminar(listCargos.getSelectedValue())) {
					JOptionPane.showMessageDialog(pc,
							"El estado \"" + listCargos.getSelectedValue() + "\" fue eliminada con \u00e9xito.",
							"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
					dlm.removeElement(listCargos.getSelectedValue());
				}
			}
		});

		editar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EditarPersonalCargoDialog((JFrame) VentanaPrincipal.getWindows()[0],pc,listCargos.getSelectedValue());
			}
		});
	}

	public void actualizar() {
		listaCargos = personalCargoController.obtenerPersonalCargos();
		dlm = new DefaultListModel<PersonalCargo>();
		for (int i = 0; i < listaCargos.size(); i++)
			dlm.addElement(listaCargos.get(i));
		listCargos.setModel(dlm);
	}
}