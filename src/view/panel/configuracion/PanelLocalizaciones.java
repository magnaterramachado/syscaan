package view.panel.configuracion;

import javax.persistence.PersistenceException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.GridBagConstraints;
import javax.swing.tree.DefaultTreeModel;
import controller.LocalidadController;
import controller.PaisController;
import controller.ProvinciaController;
import model.Localidad;
import model.Pais;
import model.Provincia;
import view.VentanaPrincipal;
import view.dialog.configuracion.AgregarProvinciaDialog;
import view.dialog.configuracion.AgregarPaisDialog;
import view.dialog.configuracion.AgregarLocalidadDialog;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.border.BevelBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import java.awt.Color;
import java.awt.Dimension;

public class PanelLocalizaciones extends JPanel {
	private static final long serialVersionUID = -4566133223626063310L;
	private JTree tree;
	private JButton agregar, remover;
	private PaisController paisController;
	private ProvinciaController provinciaController;
	private LocalidadController localidadController;
	private List<Pais> paises;
	private PanelLocalizaciones pl;

	public PanelLocalizaciones() {
		setLayout(new GridBagLayout());
		paisController = PaisController.getInstance();
		provinciaController = ProvinciaController.getInstance();
		localidadController = LocalidadController.getInstance();
		cargarComponentes();
		actualizar();
		actionListeners();
		pl = this;
	}

	private void cargarComponentes() {
		tree = new JTree();
		tree.setBorder(new BevelBorder(BevelBorder.RAISED, Color.DARK_GRAY, null, null, null));
		tree.setPreferredSize(new Dimension(300, tree.getHeight()));
		GridBagConstraints gbc_tree = new GridBagConstraints();
		gbc_tree.gridx = 0;
		gbc_tree.gridy = 0;
		gbc_tree.gridwidth = 1;
		gbc_tree.gridheight = 2;
		gbc_tree.weightx = 0.1;
		gbc_tree.weighty = 1.0;
		gbc_tree.anchor = GridBagConstraints.NORTHWEST;
		gbc_tree.fill = GridBagConstraints.VERTICAL;
		gbc_tree.insets = new Insets(15, 15, 15, 15);
		add(tree, gbc_tree);

		agregar = new JButton("Agregar localizaci\u00f3n", new ImageIcon("icon/agregar20x20.png"));
		GridBagConstraints gbc_agregar = new GridBagConstraints();
		gbc_agregar.gridx = 1;
		gbc_agregar.gridy = 0;
		gbc_agregar.gridwidth = 1;
		gbc_agregar.gridheight = 1;
		gbc_agregar.weightx = 0.9;
		gbc_agregar.weighty = 0.0;
		gbc_agregar.anchor = GridBagConstraints.NORTHWEST;
		gbc_agregar.fill = GridBagConstraints.NONE;
		gbc_agregar.insets = new Insets(15, 0, 5, 0);
		add(agregar, gbc_agregar);

		remover = new JButton("Remover localizaci\u00f3n", new ImageIcon("icon/remover20x20.png"));
		GridBagConstraints gbc_remover = new GridBagConstraints();
		gbc_remover.gridx = 1;
		gbc_remover.gridy = 1;
		gbc_remover.gridwidth = 1;
		gbc_remover.gridheight = 1;
		gbc_remover.weightx = 0.9;
		gbc_remover.weighty = 1.0;
		gbc_remover.anchor = GridBagConstraints.NORTHWEST;
		gbc_remover.fill = GridBagConstraints.NONE;
		gbc_remover.insets = new Insets(0, 0, 0, 0);
		add(remover, gbc_remover);
		remover.setEnabled(false);

		remover.setPreferredSize(remover.getPreferredSize());
		agregar.setPreferredSize(remover.getPreferredSize());
	}

	@SuppressWarnings("serial")
	public void actualizar() {
		paises = paisController.obtenerPaises();
		tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Paises") {
			{
				List<Provincia> provincias;
				for (int i = 0; i < paises.size(); i++) {
					DefaultMutableTreeNode nodoPais = new DefaultMutableTreeNode(paises.get(i));
					add(nodoPais);
					provincias = provinciaController.obtenerProvincias(paises.get(i));
					List<Localidad> localidades;
					for (int e = 0; e < provincias.size(); e++) {
						DefaultMutableTreeNode nodoProvincia = new DefaultMutableTreeNode(provincias.get(e));
						nodoPais.add(nodoProvincia);
						localidades = localidadController.obtenerLocalidades(provincias.get(e));
						for (int j = 0; j < localidades.size(); j++) {
							DefaultMutableTreeNode nodoLocalidad = new DefaultMutableTreeNode(localidades.get(j));
							nodoProvincia.add(nodoLocalidad);
						}
					}
				}
			}
		}));

		for (int i = 0; i < tree.getRowCount(); i++)
			tree.expandRow(i);
	}

	private void actionListeners() {
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				if (tree.getSelectionPath() != null
						&& (tree.getSelectionPath().getPathCount() >= 2 && tree.getSelectionPath().getPathCount() <= 4))
					remover.setEnabled(true);
				else
					remover.setEnabled(false);

				if (tree.getSelectionPath() != null
						&& (tree.getSelectionPath().getLastPathComponent().toString().equals("Argentina")
								|| tree.getSelectionPath().getLastPathComponent().toString().equals("Buenos Aires")
								|| tree.getSelectionPath().getLastPathComponent().toString().equals("Necochea")
								|| tree.getSelectionPath().getLastPathComponent().toString().equals("Paises")))
					remover.setEnabled(false);
				else
					remover.setEnabled(true);
			}
		});

		agregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int posPaisSeleccionado = 0;
				List<Provincia> provincias = null;

				if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() != 1
						&& tree.getSelectionPath().getPathCount() != 2)
					for (int i = 0; i < paises.size(); i++)
						if (paises.get(i).getPais().equals(tree.getSelectionPath().getLastPathComponent().toString())
								|| paises.get(i).getPais().equals(
										tree.getSelectionPath().getParentPath().getLastPathComponent().toString())
								|| paises.get(i).getPais().equals(tree.getSelectionPath().getParentPath()
										.getParentPath().getLastPathComponent().toString())) {
							provincias = provinciaController.obtenerProvincias(paises.get(i));
							posPaisSeleccionado = i;
						}

				if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() == 2) {
					for (int i = 0; i < paises.size(); i++)
						if (paises.get(i).getPais().equals(tree.getSelectionPath().getLastPathComponent().toString()))
							posPaisSeleccionado = i;

					new AgregarProvinciaDialog((JFrame) VentanaPrincipal.getWindows()[0], pl, paises,
							posPaisSeleccionado);
				} else if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() >= 3) {
					int posProvinciaSeleccionada = 0;
					for (int i = 0; i < provincias.size(); i++)
						if (provincias.get(i).getProvincia()
								.equals(tree.getSelectionPath().getLastPathComponent().toString()))
							posProvinciaSeleccionada = i;
					new AgregarLocalidadDialog((JFrame) VentanaPrincipal.getWindows()[0], pl, paises,
							posPaisSeleccionado, provincias, posProvinciaSeleccionada);
				} else
					new AgregarPaisDialog((JFrame) VentanaPrincipal.getWindows()[0], pl);
			}
		});

		remover.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() == 2) {
						if (paisController.eliminar(paisController
								.obtenerPais(tree.getSelectionPath().getLastPathComponent().toString()))) {
							JOptionPane.showMessageDialog(getParent(),
									"El pa\u00eds \"" + tree.getSelectionPath().getLastPathComponent().toString()
											+ "\" fue eliminado con \u00e9xito.",
									"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
							actualizar();
						}
					} else if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() == 3) {
						for (int i = 0; i < paises.size(); i++)
							if (paises.get(i).getPais()
									.equals(tree.getSelectionPath().getParentPath().getLastPathComponent().toString())
									&& provinciaController.eliminar(provinciaController.obtenerProvincia(
											tree.getSelectionPath().getLastPathComponent().toString(),
											paises.get(i)))) {
								JOptionPane.showMessageDialog(pl,
										"La provincia de \"" + tree.getSelectionPath().getLastPathComponent().toString()
												+ "\" fue eliminada con \u00e9xito.",
										"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
								actualizar();
							}
					} else if (tree.getSelectionPath() != null && tree.getSelectionPath().getPathCount() == 4) {
						for (int i = 0; i < paises.size(); i++)
							if (paises.get(i).getPais().equals(tree.getSelectionPath().getParentPath().getParentPath()
									.getLastPathComponent().toString())) {
								Provincia provincia = provinciaController.obtenerProvincia(
										tree.getSelectionPath().getParentPath().getLastPathComponent().toString(),
										paises.get(i));

								if (localidadController.eliminar(localidadController.obtenerLocalidad(
										tree.getSelectionPath().getLastPathComponent().toString(), provincia))) {
									JOptionPane.showMessageDialog(pl,
											"La localidad de \""
													+ tree.getSelectionPath().getLastPathComponent().toString()
													+ "\" fue eliminada con \u00e9xito.",
											"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
									actualizar();
								}
							}
					} else
						JOptionPane.showMessageDialog(pl,
								"No eligi\u00f3 ning\u00fan pa\u00eds, provincia ni localidad.\nSeleccione una opci\u00f3n v\u00e1lida",
								"Acci\u00f3n imposible", JOptionPane.WARNING_MESSAGE);
				} catch (PersistenceException e1) {
					JOptionPane.showMessageDialog(pl,
							"Existen localizaciones contenidas dentro del objeto que quiere eliminar.\nElimine primero las localizaciones contenidas.",
							"Acci\u00f3n imposible", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}