package view.panel.configuracion;

import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import controller.PerroRazaController;
import model.PerroRaza;
import view.VentanaPrincipal;
import view.dialog.configuracion.AgregarPerroRazaDialog;
import view.dialog.configuracion.EditarPerroRazaDialog;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.GridBagConstraints;

public class PanelRazasPerro extends JPanel {

	private static final long serialVersionUID = -9162110791621444898L;
	private PanelRazasPerro pr;
	private JList<PerroRaza> listRazas;
	private DefaultListModel<PerroRaza> dlm;
	private List<PerroRaza> listaRazas;
	private JButton agregar, remover, editar;
	private PerroRazaController perroRazaController;

	public PanelRazasPerro() {
		perroRazaController = PerroRazaController.getInstance();
		perroRazaController.controlarRazasPorDefecto();
		setLayout(new GridBagLayout());
		cargarComponentes();
		actualizar();
		actionListeners();
		pr = this;
	}

	private void cargarComponentes() {
		listRazas = new JList<PerroRaza>();
		JScrollPane scroll = new JScrollPane(listRazas);
		scroll.setPreferredSize(new Dimension(150, scroll.getHeight()));
		GridBagConstraints gbc_listRazas = new GridBagConstraints();
		gbc_listRazas.gridx = 0;
		gbc_listRazas.gridy = 0;
		gbc_listRazas.gridwidth = 1;
		gbc_listRazas.gridheight = 4;
		gbc_listRazas.weightx = 0.2;
		gbc_listRazas.weighty = 1.0;
		gbc_listRazas.anchor = GridBagConstraints.NORTHWEST;
		gbc_listRazas.fill = GridBagConstraints.BOTH;
		gbc_listRazas.insets = new Insets(15, 15, 200, 40);
		add(scroll, gbc_listRazas);

		agregar = new JButton("Agregar raza", new ImageIcon("icon/agregar20x20.png"));
		GridBagConstraints gbc_agregar = new GridBagConstraints();
		gbc_agregar.gridx = 1;
		gbc_agregar.gridy = 0;
		gbc_agregar.gridwidth = 1;
		gbc_agregar.gridheight = 1;
		gbc_agregar.weightx = 0.8;
		gbc_agregar.weighty = 0.0;
		gbc_agregar.anchor = GridBagConstraints.NORTHWEST;
		gbc_agregar.fill = GridBagConstraints.NONE;
		gbc_agregar.insets = new Insets(15, 0, 5, 0);
		add(agregar, gbc_agregar);

		remover = new JButton("Remover raza", new ImageIcon("icon/remover20x20.png"));
		GridBagConstraints gbc_remover = new GridBagConstraints();
		gbc_remover.gridx = 1;
		gbc_remover.gridy = 1;
		gbc_remover.gridwidth = 1;
		gbc_remover.gridheight = 1;
		gbc_remover.weightx = 0.8;
		gbc_remover.weighty = 0.0;
		gbc_remover.anchor = GridBagConstraints.NORTHWEST;
		gbc_remover.fill = GridBagConstraints.NONE;
		gbc_remover.insets = new Insets(0, 0, 5, 0);
		add(remover, gbc_remover);
		remover.setEnabled(false);

		editar = new JButton("Editar raza", new ImageIcon("icon/modificar20x20.png"));
		GridBagConstraints gbc_editar = new GridBagConstraints();
		gbc_editar.gridx = 1;
		gbc_editar.gridy = 3;
		gbc_editar.gridwidth = 1;
		gbc_editar.gridheight = 1;
		gbc_editar.weightx = 0.9;
		gbc_editar.weighty = 1.0;
		gbc_editar.anchor = GridBagConstraints.NORTHWEST;
		gbc_editar.fill = GridBagConstraints.NONE;
		gbc_editar.insets = new Insets(0, 0, 0, 0);
		add(editar, gbc_editar);
		editar.setEnabled(false);

		editar.setPreferredSize(remover.getPreferredSize());
		remover.setPreferredSize(remover.getPreferredSize());
		agregar.setPreferredSize(remover.getPreferredSize());
	}

	private void actionListeners() {
		listRazas.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (listRazas.getSelectedValue() != null && listRazas.getSelectedValue().isEliminable()) {
					remover.setEnabled(true);
					editar.setEnabled(true);
				} else {
					remover.setEnabled(false);
					editar.setEnabled(false);
				}
			}
		});
		
		agregar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AgregarPerroRazaDialog((JFrame) VentanaPrincipal.getWindows()[0],pr);
			}
		});

		remover.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (perroRazaController.eliminar(listRazas.getSelectedValue())) {
					JOptionPane.showMessageDialog(pr,
							"La raza \"" + listRazas.getSelectedValue() + "\" fue eliminada con \u00e9xito.",
							"Acci\u00f3n concretada", JOptionPane.INFORMATION_MESSAGE);
					dlm.removeElement(listRazas.getSelectedValue());
				}
			}
		});
		
		editar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EditarPerroRazaDialog((JFrame) VentanaPrincipal.getWindows()[0],pr,listRazas.getSelectedValue());
			}
		});
	}

	public void actualizar() {
		listaRazas = perroRazaController.obtenerRazas();
		dlm = new DefaultListModel<PerroRaza>();
		for (int i = 0; i < listaRazas.size(); i++) {
			listaRazas.get(i).setEtiqueta(true);
			dlm.addElement(listaRazas.get(i));
		}
		listRazas.setModel(dlm);
	}
}