package view.panel.administracion;

import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

import controller.PersonalController;
import model.Personal;

public class PanelPersonal extends JPanel{
	private static final long serialVersionUID = -9162110791621444898L;
	private PanelPersonal pp;
	private JList<Personal> listPersonal;
	private JTable tblPersonal;
	private JTableHeader tblHdrPersonal;
	private JButton agregar, remover, editar;
	private PersonalController personalController;

	public PanelPersonal() {
		personalController = PersonalController.getInstance();
		setLayout(new GridBagLayout());
		cargarComponentes();
		actualizar();
		actionListeners();
		pp = this;
	}

	private void cargarComponentes() {
		
	}
	
	private void actualizar() {
		
	}
	
	private void actionListeners() {
		
	}
}
