package view.panel.administracion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import controller.PerroController;
import model.Perro;
import view.VentanaPrincipal;
import view.dialog.administracion.AgregarPerroDialog;
import view.dialog.administracion.EditarPerroDialog;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import admin.SistemaCAAN;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PanelPerros extends JPanel {

	private static final long serialVersionUID = 4122907085936468481L;
	private PanelPerros pp;
	private JPanel panelBotones, panelTable;
	private JTable tbPerros;
	private List<Perro> listaPerros;
	private PerroController perroController;
	private JButton btAltaPerro, btEliminarPerro, btEditarPerro;
	private JScrollPane scroll;

	public PanelPerros() {
		pp = this;
		perroController = PerroController.getInstance();
		listaPerros = perroController.obtenerPerros();
		setLayout(new GridBagLayout());
		cargarComponentes();
		actualizar();
		actionListeners();
	}

	private void cargarComponentes() {
		panelBotones = new JPanel();
		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelBotones.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelBotones = new GridBagConstraints();
		gbc_panelBotones.gridx = 0;
		gbc_panelBotones.gridy = 0;
		gbc_panelBotones.gridwidth = 1;
		gbc_panelBotones.gridheight = 1;
		gbc_panelBotones.weightx = 1.0;
		gbc_panelBotones.weighty = 0.3;
		gbc_panelBotones.anchor = GridBagConstraints.NORTH;
		gbc_panelBotones.fill = GridBagConstraints.BOTH;
		gbc_panelBotones.insets = new Insets(15, 10, 15, 30);
		add(panelBotones, gbc_panelBotones);
		Dimension d1 = new Dimension(panelBotones.getPreferredSize());

		panelTable = new JPanel();
		panelTable.setLayout(new GridBagLayout());
		GridBagConstraints gbc_panelTable = new GridBagConstraints();
		gbc_panelTable.gridx = 0;
		gbc_panelTable.gridy = 1;
		gbc_panelTable.gridwidth = 1;
		gbc_panelTable.gridheight = 1;
		gbc_panelTable.weightx = 1.0;
		gbc_panelTable.weighty = 0.7;
		gbc_panelTable.anchor = GridBagConstraints.NORTH;
		gbc_panelTable.fill = GridBagConstraints.BOTH;
		add(panelTable, gbc_panelTable);
		Dimension d2 = new Dimension(panelBotones.getPreferredSize());

		crearTabla();
		crearBotonera();
		panelBotones.setPreferredSize(d1);
		panelTable.setPreferredSize(d2);
	}

	private void crearBotonera() {
		btAltaPerro = new JButton("Ingresar perro", new ImageIcon("icon/agregar20x20.png"));
		GridBagConstraints gbc_btAltaPerro = new GridBagConstraints();
		gbc_btAltaPerro.gridx = 0;
		gbc_btAltaPerro.gridy = 0;
		gbc_btAltaPerro.gridwidth = 1;
		gbc_btAltaPerro.gridheight = 1;
		gbc_btAltaPerro.weightx = 0.0;
		gbc_btAltaPerro.weighty = 1.0;
		gbc_btAltaPerro.anchor = GridBagConstraints.NORTHWEST;
		gbc_btAltaPerro.fill = GridBagConstraints.NONE;
		gbc_btAltaPerro.insets = new Insets(15, 15, 15, 15);
		panelBotones.add(btAltaPerro, gbc_btAltaPerro);

		btEliminarPerro = new JButton("Eliminar perro", new ImageIcon("icon/remover20x20.png"));
		btEliminarPerro.setEnabled(false);
		GridBagConstraints gbc_btEliminarPerro = new GridBagConstraints();
		gbc_btEliminarPerro.gridx = 1;
		gbc_btEliminarPerro.gridy = 0;
		gbc_btEliminarPerro.gridwidth = 1;
		gbc_btEliminarPerro.gridheight = 1;
		gbc_btEliminarPerro.weightx = 0.0;
		gbc_btEliminarPerro.weighty = 1.0;
		gbc_btEliminarPerro.anchor = GridBagConstraints.NORTHWEST;
		gbc_btEliminarPerro.fill = GridBagConstraints.NONE;
		gbc_btEliminarPerro.insets = new Insets(15, 0, 15, 15);
		panelBotones.add(btEliminarPerro, gbc_btEliminarPerro);

		btEditarPerro = new JButton("Editar perro", new ImageIcon("icon/modificar20x20.png"));
		btEditarPerro.setEnabled(false);
		GridBagConstraints gbc_btEditarPerro = new GridBagConstraints();
		gbc_btEditarPerro.gridx = 2;
		gbc_btEditarPerro.gridy = 0;
		gbc_btEditarPerro.gridwidth = 1;
		gbc_btEditarPerro.gridheight = 1;
		gbc_btEditarPerro.weightx = 1.0;
		gbc_btEditarPerro.weighty = 1.0;
		gbc_btEditarPerro.anchor = GridBagConstraints.NORTHWEST;
		gbc_btEditarPerro.fill = GridBagConstraints.NONE;
		gbc_btEditarPerro.insets = new Insets(15, 0, 15, 15);
		panelBotones.add(btEditarPerro, gbc_btEditarPerro);

		btAltaPerro.setPreferredSize(btAltaPerro.getPreferredSize());
		btEliminarPerro.setPreferredSize(btAltaPerro.getPreferredSize());
		btEditarPerro.setPreferredSize(btAltaPerro.getPreferredSize());
	}

	private void crearTabla() {
		tbPerros = new JTable();
		tbPerros.setModel(new PerroTableModel());
		tbPerros.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tbPerros.setBorder(new EmptyBorder(0, 0, 0, 0));
		tbPerros.setGridColor(new Color(189, 224, 242));
		tbPerros.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tbPerros.setAutoCreateRowSorter(true);

		scroll = new JScrollPane(tbPerros);
		GridBagConstraints gbc_tbPerros = new GridBagConstraints();
		gbc_tbPerros.gridx = 0;
		gbc_tbPerros.gridy = 0;
		gbc_tbPerros.gridwidth = 1;
		gbc_tbPerros.gridheight = 1;
		gbc_tbPerros.weightx = 1.0;
		gbc_tbPerros.weighty = 1.0;
		gbc_tbPerros.anchor = GridBagConstraints.CENTER;
		gbc_tbPerros.fill = GridBagConstraints.BOTH;
		gbc_tbPerros.insets = new Insets(15, 10, 91, 30);
		panelTable.add(scroll, gbc_tbPerros);
	}

	private void actionListeners() {

		tbPerros.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (tbPerros.getSelectedRows().length >= 1)
					btEliminarPerro.setEnabled(true);
				else
					btEliminarPerro.setEnabled(false);

				if (tbPerros.getSelectedRows().length == 1)
					btEditarPerro.setEnabled(true);
				else
					btEditarPerro.setEnabled(false);
			}
		});

		btAltaPerro.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AgregarPerroDialog((JFrame) VentanaPrincipal.getWindows()[0], pp);
			}
		});

		btEditarPerro.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] seleccion = tbPerros.getSelectedRows();
				for (int i = 0; i < seleccion.length; i++)
					seleccion[i] = (int) tbPerros.getModel().getValueAt(seleccion[i], 0);

				new EditarPerroDialog((JFrame) VentanaPrincipal.getWindows()[0], pp,
						perroController.obtenerPerro(seleccion[0]));
			}
		});

		btEliminarPerro.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] seleccion = tbPerros.getSelectedRows();
				String eliminacion = "";
				String nombre = "";
				for (int i = 0; i < seleccion.length; i++) {
					nombre = (String) tbPerros.getModel().getValueAt(seleccion[i], 1);
					seleccion[i] = (int) tbPerros.getModel().getValueAt(seleccion[i], 0);
					eliminacion += "[" + seleccion[i] + "]  " + nombre + "\n";
				}

				if (JOptionPane.showConfirmDialog(null,
						"Los siguientes perros ser\u00e1n eliminados:\n" + eliminacion
								+ "\n�Desea confirmar la operaci\u00f3n?.",
						"Confirmaci\u00f3n.", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					for (int i = 0; i < seleccion.length; i++)
						perroController.eliminar(perroController.obtenerPerro(seleccion[i]));
					actualizar();
				}
			}
		});
	}

	public void actualizar() {
		listaPerros = perroController.obtenerPerros();
		tbPerros.setModel(new PerroTableModel());
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_LEGAJO).setPreferredWidth(45);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_NOMBRE).setPreferredWidth(120);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_FEC_ING).setPreferredWidth(80);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_SECTOR).setPreferredWidth(45);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_RESPONSABLE).setPreferredWidth(120);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_GENERO).setPreferredWidth(45);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_ESTADO).setPreferredWidth(70);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_RAZA).setPreferredWidth(70);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_EDAD).setPreferredWidth(95);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_COLOR).setPreferredWidth(60);
		tbPerros.getColumnModel().getColumn(PerroTableModel.COLUMN_OBS).setPreferredWidth(210);
		DefaultTableCellRenderer cellRenderCenter = new DefaultTableCellRenderer();
		cellRenderCenter.setHorizontalAlignment(SwingConstants.CENTER);

		for (int i = 0; i < tbPerros.getColumnCount(); i++)
			if (tbPerros.getModel().getColumnClass(i) == Integer.class)
				tbPerros.getColumnModel().getColumn(i).setCellRenderer(cellRenderCenter);

		tbPerros.getTableHeader().setDefaultRenderer(new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 138526862081402458L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int col) {
				Component c2 = new JTable().getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table,
						value, isSelected, hasFocus, row, col);
				if (table.getRowCount() > 0) {
					Component c3 = table.getCellRenderer(0, col).getTableCellRendererComponent(table, value, isSelected,
							hasFocus, 0, col);
					if (c2 instanceof JLabel && c3 instanceof JLabel)
						if (tbPerros.getModel().getColumnClass(col) == Integer.class)
							((JLabel) c2).setHorizontalAlignment(SwingConstants.CENTER);
				}
				return c2;
			}
		});

	}

	class PerroTableModel extends AbstractTableModel {
		private static final long serialVersionUID = -7146741980252244579L;
		private static final int COLUMN_LEGAJO = 0;
		private static final int COLUMN_NOMBRE = 1;
		private static final int COLUMN_FEC_ING = 2;
		private static final int COLUMN_SECTOR = 3;
		private static final int COLUMN_RESPONSABLE = 4;
		private static final int COLUMN_GENERO = 5;
		private static final int COLUMN_ESTADO = 6;
		private static final int COLUMN_RAZA = 7;
		private static final int COLUMN_EDAD = 8;
		private static final int COLUMN_COLOR = 9;
		private static final int COLUMN_OBS = 10;

		private String[] encabezados = { "Legajo", "Nombre", "Fecha ingreso", "Sector", "Responsable Asignado",
				"Genero", "Estado", "Raza", "Edad aproximada", "Color", "Observaciones" };

		private Class<?>[] columnTypes = new Class[] { Integer.class, String.class, Object.class, Integer.class,
				String.class, String.class, String.class, String.class, Integer.class, String.class, String.class };

		public PerroTableModel() {
		}

		@Override
		public String getColumnName(int columnIndex) {
			return encabezados[columnIndex];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return columnTypes[columnIndex];
		}

		@Override
		public int getRowCount() {
			return listaPerros.size();
		}

		@Override
		public int getColumnCount() {
			return encabezados.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (listaPerros.get(rowIndex) == null)
				return null;
			Perro perro = listaPerros.get(rowIndex);
			Object returnValue = null;
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

			switch (columnIndex) {
			case COLUMN_LEGAJO:
				returnValue = perro.getNumLegajo();
				break;
			case COLUMN_NOMBRE:
				returnValue = perro.getNombre();
				break;
			case COLUMN_FEC_ING:
				returnValue = df.format(perro.getFechaIngreso());
				break;
			case COLUMN_SECTOR:
				returnValue = perro.getSector().getNumSector();
				break;
			case COLUMN_RESPONSABLE:
				if (perro.getResponsable() != null)
					returnValue = perro.getResponsable().getApellido() + " " + perro.getResponsable().getNombre();
				else
					returnValue = null;
				break;
			case COLUMN_GENERO:
				returnValue = (perro.getGenero() == 'H') ? "Hembra" : "Macho";
				break;
			case COLUMN_ESTADO:
				returnValue = perro.getEstado().getDescripcion();
				break;
			case COLUMN_RAZA:
				returnValue = perro.getRaza().getRaza();
				break;
			case COLUMN_EDAD:
				returnValue = SistemaCAAN.calcularEdad(perro.getFechaNacimiento());
				break;
			case COLUMN_COLOR:
				returnValue = perro.getColor();
				break;
			case COLUMN_OBS:
				returnValue = perro.getObservacion();
				break;

			default:
				throw new IllegalArgumentException("Invalid column index");
			}

			return returnValue;
		}
	}
}