package admin;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import controller.LocalidadController;
import controller.PerroEstadoController;
import controller.PerroRazaController;
import util.HibernateUtil;
import view.VentanaPrincipal;

public class SistemaCAAN {

	public static final Date fechaHoy = new Date();
	public static final String nombreSistema = "SistemaCAAN";
	public static final Integer version = 1;
	public static final Integer subversion = 0;

	public static void main(String[] args) {

		/*
		 * � \u00e1 � \u00c1 � \u00e9 � \u00c9 � \u00ed � \u00cd � \u00f3 � \u00d3 �
		 * \u00fa � \u00da � \u00f1 � \u00d1
		 */
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		System.out.println(sdf.format(fechaHoy));

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//			UIManager.setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		new VentanaPrincipal(nombreSistema, version, subversion);
		HibernateUtil.crearSessionFactory(HibernateUtil.SERVER_CFG);
		cargarBaseDeDatos();
	}

	private static void cargarBaseDeDatos() {
		LocalidadController.getInstance().controlarLocalidadesPorDefecto();
		PerroEstadoController.getInstance().controlarEstadosPorDefecto();
		PerroRazaController.getInstance().controlarRazasPorDefecto();
	}

	public static Integer convertInt(String numero) {
		Integer numeroConvertido = null;
		try {
			numeroConvertido = Integer.parseInt(numero.trim());
			return numeroConvertido;
		} catch (NumberFormatException nfe) {
			return null;
		}
	}

	public static Integer calcularEdad(Date fechaNacimiento) {
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		LocalDate fechaNac = LocalDate.parse(sdf.format(fechaNacimiento), fmt);
		LocalDate ahora = LocalDate.parse(sdf.format(fechaHoy), fmt);
		Period edad = Period.between(fechaNac, ahora);
		return edad.getYears();
	}
}